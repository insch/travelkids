<?php 

function clean($dirty, $encoding = 'UTF-8') {
    return htmlspecialchars(
        strip_tags($dirty),
        ENT_QUOTES | ENT_HTML5,
        $encoding
    );
}

function purify($dirty) {
    $config = HTMLPruifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    
    return $purifier->purify($dirty);
}