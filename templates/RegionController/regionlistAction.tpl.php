<div class="wrapperList">
    <h2>Liste aller Reisen der Region: <?= $region->getName();?></h2>

    <?php
    if (count($region->getTravels())) {
        foreach ($region->getTravels() as $k => $travel) { ?>
            <?php $travelstart = ($travel->getTravelstart()->getDatetime()->getTimestamp()) ; 
            $now = (new \DateTime())->getTimestamp();
            if($travelstart > $now) { ?>
                <div class='reiseVorschau'>
                    <div class='reiseImg'>
                        <a href='<?= BASE_DIR . '/index.php?controller=travel&action=traveldetail&id=' . $travel->getId(); ?>'>
                            <img src='<?= BASE_DIR . '/img/thumbnail/' . $travel->getThumbnail(); ?>' alt=''/> 
                        </a>
                        <a class='kategorie' href='<?= BASE_DIR . '/index.php?controller=category&action=catlist&id=' . $travel->getCategory()->getId(); ?>'> <?= $travel->getCategory()->getTitle(); ?></a>
                    </div>
                    <div class='reiseInfos'>
                        <h3><?= $travel->getTitle(); ?></h3>
                        <p><?= $travel->getTeaser(); ?></p>
                        <p>Reisezeit: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></p>
                        <p><a href='<?= BASE_DIR . '/index.php?controller=travel&action=traveldetail&id=' . $travel->getId(); ?>' title='<?= $travel->getTitle(); ?>'>Details - <?= $travel->getTitle(); ?></a></p>
                    </div>
                    <div class='reiseBtn'>
                        <div class='vorschauPreis'><?= $travel->getPrice() . '€ pro Person'; ?></div>
                        <a href='<?= BASE_DIR . '/index.php?controller=booking&action=form&id=' . $travel->getId()?>' title='Reise buchen'><button>Zur Buchung</button></a>
                    </div>
                </div>
        <?php } }
    } else { 
        'Keine Reisen für diese Region gefunden';
    } ?>
</div>