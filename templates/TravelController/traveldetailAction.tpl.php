<!--<div class="row">
        <a href="<?php // echo BASE_DIR.'/admin/index.php?controller=travel&action=form&id='.$travel->getId();  ?>" class="btn btn-xs btn-warning pull-right"><span class="glyphicon glyphicon-wrench"></span> bearbeiten</a>
</div>-->

<div class='wrapperDetail'>
    <header id='header'>
        <div class='detail'>
            <div class='detailBild'>
                <img src='<?= BASE_DIR; ?>/img/detail/<?= $travel->getImage(); ?>' alt='<?= $travel->getTitle(); ?>'>
            </div>
            <div class='teaser teaserDetail'>
                <h3 class='titel meer'><?= $travel->getTitle(); ?></h3>
                <p class='teaserPreis'><span class='preisProPerson'><?= $travel->getPrice() . '€'; ?></span><br><span class='teaserPreisEdit'>pro Person</span></p>
            </div>
            <a href='<?= BASE_DIR; ?>/index.php?controller=booking&action=form&id=<?= $travel->getId() ?>'><button class='teaserBtn'>Reise buchen</button></a>
        </div>
    </header>

    <div class='content' id='detailContent'>

        <main class='langeBeschreibung'>
            <button id='back-to-Btn' type='button' onclick='history.back()'>zurück</button>
            
            <div class="description">
                <br>
                <p><?= $travel->getTeaser(); ?></p>
                <div id='dscBild'>
                    <img src='<?= BASE_DIR.'/img/thumbnail/basteln.jpg';?>' alt='Toll hier'><img src='<?= BASE_DIR.'/img/thumbnail/strohkino.jpg';?>' alt='Kino'><img src='<?= BASE_DIR.'/img/thumbnail/boys.jpg';?>' alt='jungs'><img src='<?= BASE_DIR.'/img/thumbnail/spielzimmer.jpg';?>' alt='spielzimmer'><img src='<?= BASE_DIR.'/img/thumbnail/zimmer1.jpg';?>' alt='zimmer'><img src='<?= BASE_DIR.'/img/thumbnail/spielburg.jpg';?>' alt='burg'>
                </div>
                <div id='booking-mobile'>
                    <p><?= $travel->getPrice() . '€'; ?><span>pro Person</span></p>
                    <a href='<?= BASE_DIR; ?>/index.php?controller=booking&action=form&id=<?= $travel->getId() ?>'><button class='detailBtn'>Reise buchen</button></a>
                </div>

                <?= $travel->getDescription(); ?>
                <div id='dscBild'>
                    <img src='<?= BASE_DIR.'/img/thumbnail/selfie-natur.jpg';?>' alt='Toll hier'><img src='<?= BASE_DIR.'/img/thumbnail/schaf-thumbnail.jpg';?>' alt='Tiere'><img src='<?= BASE_DIR.'/img/thumbnail/schaukeln.jpg';?>' alt='Tiere'><img src='<?= BASE_DIR.'/img/thumbnail/reiseplanung.jpg';?>' alt='planung'><img src='<?= BASE_DIR.'/img/thumbnail/kuh-thumbnail.jpg';?>' alt='Tiere'><img src='<?= BASE_DIR.'/img/thumbnail/pfau-thumbnail.jpg';?>' alt='Tiere'>
                </div>
                <br>
            </div>

            <p>Jetzt für <?= $travel->getPrice() . '€'; ?><span> pro Person</span></p>
            <a href='<?= BASE_DIR; ?>/index.php?controller=booking&action=form&id=<?= $travel->getId() ?>'><button class='btns btn-m'>Reise buchen</button></a>
        </main>
        <aside>
            <div class='kurzKnapp'>
                <h3>Eure Reiseübersicht:</h3>
                <ul>
                    <i class='fa fa-calendar fa-2x' aria-hidden='true'></i> <li> Reisezeit: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></li>
                    <i class='fa fa-map-marker fa-2x' aria-hidden='true'></i> <li> Region: <a href="<?= BASE_DIR . '/index.php?controller=region&action=regionlist&id=' . $travel->getRegion()->getId(); ?>"> <?= $travel->getRegion()->getName(); ?></a></li>
                    <i class='fa fa-users fa-2x' aria-hidden='true'></i> <li> max. Personen: 8</li>
                    <i class='fa fa-map fa-2x' aria-hidden='true'></i> <li> Anfahrt planen </li>
                    <?php if (isset($_SESSION['user'])) { ?>
                    <i class='fa fa-cog fa-2x' aria-hidden='true'></i> <li><a href='<?= BASE_DIR .'/admin/index.php?controller=travel&action=edit&id='.$travel->getId();?>'> Reise editieren</a></li>
                    <?php } ?>
                </ul>
            </div>
        </aside>
    </div>
</div>
