<?php

namespace Controllers\Backend;

class RegionController extends \Controllers\AbstractBase {

    public function indexAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $this->redirect('list', 'region');
        } else {
            $this->redirect('login', 'user');
        }
    }

    // admin/index.php?controller=category&action=list
    public function listAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $regions = $this->em->getRepository('\Entities\Region')->findAll();

            $this->addContext('regions', $regions);
            $this->setTemplate('listAction');
        } else {
            $this->redirect('login', 'user');
        }
    }

}
