# README
Reisebüroprojekt für die Abschlussarbeit bei Webmasters Europe.
Eingereicht und selbst verfasst von Inga Schünemann am 02.10.2017

Um das Projekt zu starten, werden Extensions und/oder Bibliotheken benötigt, die über den Composer installiert werden.
Genutzt wurde die Installationsmethode Install(dev) und das Projekt wurde mit dem IDE Netbeans 8.2 geschrieben.
Zudem wurde MAMP als lokaler Server mit MySQL genutzt.

Unter Windows ist die Änderung der localhost-Daten notwendig.

### Datenbank befüllen ###

Um die Datenbank anzulegen 'setup.php' aufrufen und um Beispieldaten in die Datenbank zu laden, 'reset.php' aufrufen.

### Login für Mitarbeiter-Bereich ###
User: Admin
Password: root
