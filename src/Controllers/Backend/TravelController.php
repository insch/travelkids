<?php

namespace Controllers\Backend;

use Entities\Travel;
use Webmasters\Doctrine\ORM\Util;

class TravelController extends \Controllers\AbstractBase {

    public function indexAction() {
        if (self::isUserLoggedIn()) {
            $this->travellistAction();
        }
    }

    public function travellistAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $travels = $this->em->getRepository('\Entities\Travel')->findAll(); // Anzahl der Reisen
            $regions = $this->em->getRepository('\Entities\Region')->findAll();
            $categories = $this->em->getRepository('\Entities\Category')->findAll();

            $this->addContext('travels', $travels);
            $this->addContext('regions', $regions);
            $this->addContext('categories', $categories);

            $this->setTemplate('travellistAction');
        } else {
            $this->redirect('login', 'user');
        }
    }

    public function createAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $regions = $this->em->getRepository('\Entities\Region')->findAll();
            $categories = $this->em->getRepository('\Entities\Category')->findAll();

            $this->addContext('regions', $regions);
            $this->addContext('categories', $categories);
            $this->setTemplate('formAction');
        } else {
            $this->redirect('login', 'user');
        }
    }

    public function formAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $form = filter_input_array(INPUT_POST);
            if (isset($form['travelId'])) {
                $travel = $this->em->getRepository('\Entities\Travel')->find($form['travelId']);
            } else {
                $travel = new \Entities\Travel();
            }
            //save
            if($_POST) {
                $travel->setByArray($_POST);
                $validator = new \Validators\TravelValidator($travel, $this->em);
                $category = $this->em->getRepository('\Entities\Category')->find($form['categoryId']);
                $travel->setCategory($category);
                $region = $this->em->getRepository('\Entities\Region')->find($form['regionId']);
                $travel->setRegion($region);
                if($validator->isValid()) {
                    $this->em->persist($travel);
                    $this->em->flush();
                    $this->setMessage('Die Reise wurde gespeichert');
                    //show travellist page after sending the form
                    $this->redirect('travellist', 'travel');
                }
                $this->addContext('errors', $validator->getErrors());
            }
            
        } else {
            $this->redirect('login', 'user');
        }
    }

    //user is able to edit a travel-entry
    public function editAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $id = filter_input(INPUT_GET, 'id');

            $travel = $this->em->getRepository('\Entities\Travel')->find($id);
            $regions = $this->em->getRepository('\Entities\Region')->findAll();
            $categories = $this->em->getRepository('\Entities\Category')->findAll();

            $this->addContext('travel', $travel);
            $this->addContext('regions', $regions);
            $this->addContext('categories', $categories);

            $this->setTemplate('formAction');
        } else {
            $this->redirect('login', 'user');
        }
    }

    //user is able to delete a travel-entry
    public function deleteAction() {

        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $travel = $this->em->getRepository('\Entities\Travel')->find((int)$_REQUEST['id']);
            $travel || $this->render404();
            
            if($_POST) {
                $this->em->remove($travel);
                $this->em->flush();
                
                $this->setMessage('Die Reise wurde aus dem Bestand gelöscht.');
                $this->redirect('travellist', 'travel');
            }
            $this->addContext('travel', $travel);
        } else {
            $this->redirect('login', 'user');
        }
    }

}
