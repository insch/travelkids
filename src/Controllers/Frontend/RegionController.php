<?php

namespace Controllers\Frontend;

//use Webmasters\Doctrine\ORM\Util;

class RegionController extends \Controllers\AbstractBase {

    public function indexAction() {
        //Liste der Regionen auf der /index.php?controller=region&action=regionlist anzeigen
        $this->regionlistAction();
    }

    // /index.php?controller=category&action=regionlist
    public function regionlistAction() {
        $id = filter_input(INPUT_GET, 'id');
        
        $region = $this->em->getRepository('Entities\Region')->find((int)$id);
        $region || $this->render404();
        $cat = $this->em->getRepository('Entities\Category')->find((int)$id);
        
        $travels = $this->em->getRepository('Entities\Travel')->findAll();
                
        $this->addContext('region', $region);
        $this->addContext('cat', $cat);
        $this->addContext('travels', $travels);
        $this->addContext('id', $id);
        $this->setTemplate('regionlistAction');
    }

}