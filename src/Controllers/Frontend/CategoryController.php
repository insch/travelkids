<?php

namespace Controllers\Frontend;

use Webmasters\Doctrine\ORM\Util;

class CategoryController extends \Controllers\AbstractBase {

    public function indexAction() {
        //Liste der Kategorien auf der /kategorie/ anzeigen
        $this->catlistAction();
    }

    // /index.php?controller=category&action=catlist
    public function catlistAction() {
        $id = filter_input(INPUT_GET, 'id');
        $category = $this->em->getRepository('Entities\Category')->find((int)$id);
        $category || $this->render404();
                
        $region = $this->em->getRepository('Entities\Region')->findAll();

        $travels = $this->em->getRepository('Entities\Travel')->findAll();
     

        $this->addContext('category', $category);
        $this->addContext('region', $region);
        $this->addContext('travels', $travels);
//        $this->addContext('id', $id);
        $this->setTemplate('catlistAction');
    }

    // /index.php?controller=category&action=list

    public function sucheAction() {
        //zeige die Suchergebnisse aus dem Suchfeld
        $search = filter_input_array(INPUT_POST);
      
        /*         * **** QueryBuilder ***** */
        $query = $this->em
                ->createQueryBuilder()
                ->select('c')
                ->from('Entities\Category', 'c')
                ->where('c.title LIKE :search')
                ->setParameter('search', '%' . $search['search'] . '%')
                ->orderBy('c.title', 'ASC')
                ->getQuery();

        $categories = $query->getResult();

        $this->addContext('categories', $categories);
        $this->addContext('search', $search['search']);
        $this->setTemplate('listAction');
    }
}
