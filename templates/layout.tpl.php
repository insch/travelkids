<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Travelkids-Reisen</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
        <link rel="shortcut icon" type="image/x-icon" href="<?= BASE_DIR; ?>/img/favicon.ico">
        <link type="text/css" rel="stylesheet" href="<?= BASE_DIR; ?>/css/stylesheet.css">
        <link type="text/css" rel="stylesheet" href="<?= BASE_DIR; ?>/font-awesome-4.7.0/css/font-awesome.css">
        <link href="https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps|Happy+Monkey|Raleway:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">
        <script src="<?= BASE_DIR;?>/js/script.js" defer></script>
    </head>

    <body>
        <div class="wrapper">
            <?php require_once 'navi.tpl.php'; ?>


            <?php require $template; ?>

            <?php require 'flash_message.tpl.php'; ?>
            <?php require 'errors.tpl.php'; ?>
            <?php require_once 'footer.tpl.php'; ?>
            
            
        </div>
        <div id="btnUp" title="Zum Seitenanfang">❯</div>
    </body>
</html>
