<?php

namespace Controllers\Frontend;

class UserController extends \Controllers\AbstractBase {
    
    public function indexAction() {
        if (self::isUserLoggedIn()) {
            $this->redirect('index', 'reise');        
        } else {
            $this->redirect('login', 'user');
        }
    }
    
     //Validieren, ob User eingeloggt ist, um Daten zu sehen
    public static function isUserLoggedIn() {
        return(isset($_SESSION['user'])) ? true : false; //wenn ein User in der Session drin ist, dann geben wir true aus, ansonsten false
    }
        
    //Login-Forumlar anzeigen
    public function adminAction($error = false) {
        
        
        $this->addContext('error', $error);
        //Formular anzeigen
        $this->setTemplate('loginAction');   
        
    }
    
    public static function readCookie() {
        if(isset($_COOKIE['user'])) {
            $_SESSION['user'] = $_COOKIE['user'];
        } else {
            $this->redirect('login', 'user');
        }
    }
    
    public function loginAction() {
        $form = filter_input_array(INPUT_POST);
        
        $user = $this->em->getRepository('Entities\User')->findOneByUsername($form['username']); //einen Wert, der der Eingabe entspricht finden
        
        if(($user instanceof\Entities\User) && password_verify($form['password'], $user->getPassword())) {
            $_SESSION['user'] = $user->getId();
            //wurde das Häkchen in zum eingeloggt bleiben aktiviert?
            //Cookie setzen
            if (isset($form['stayLoggedIn'])) {
            setcookie('user', $user->getId(), time() + (60 * 60 * 24 * 30));
            }
            $this->redirect('travellist', 'travel');
        } else {
            $this->redirect('falselogin', 'user'); //$error = false wird in authAction als Parameter mitgegeben, damit falselogin ausgelesen werden kann
        }
        
    }
    
    public function logoutAction() {
     session_destroy();
     setcookie('user', '', time()-3600); //Überschreiben des Cookies "user", nicht reinsetzen, aktuelle Zeit minus eine Stunde, damit Cookie gelöscht wird
     $this->redirect('login', 'user');
    }

}