<div id="logoAndNav">
    <div class="logo">
        <a href="#" title="Startseite"><img src="../img/logo/travelkids-logo-180x360.png" alt="Logo Travelkids.de"></a>
    </div>
    <nav id="nav" onclick="return true">
        <ul>
            <li class='nav-item'><a href='./index.php?controller=travel&action=travellist'>Reiseübersicht</a>
            <li class='nav-item'><a href='./index.php?controller=travel&action=create'>Reise anlegen</a></li>
            <li class='nav-item'><a href='./index.php?controller=category&action=list'>Kategorien</a></li>
            <li class='nav-item'><a href='./index.php?controller=region&action=list'>Regionen</a></li>
            <li class='nav-item'><a href='./index.php?controller=booking&action=bookinglist'>Buchungen</a></li>
            <li class='nav-item'><a href='<?= BASE_DIR.'/index.php'?>'>Besucheransicht</a></li>
        </ul>
    </nav>
</div>