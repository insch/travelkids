<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Travelkids Reisen</title>
    <!-- TinyMCE WYSIWYG Editor für die Texteingabe https://www.tinymce.com/download/-->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({selector: '#description'});</script>
    <link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
    <link href="https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps|Happy+Monkey|Raleway:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <?php require_once 'adminnavi.tpl.php'; ?>
    
            <?php
            require $template;
            ?>

        <?php require 'flash_message.tpl.php'; ?>
        <?php require 'errors.tpl.php'; ?>
        <?php require_once 'footer.tpl.php'; ?>
    </div>
</body>
</html>