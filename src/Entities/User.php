<?php

namespace Entities;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Webmasters\Doctrine\ORM\Util;

// DON'T forget this use statement!!!
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Doctrine\ORM\Mapping\Entity
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends AbstractEntity {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned" = true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;
    
     /**
     *
     * @ORM\Column(type="string", length=255)
     */
    private $password;
    
    
    /* *** GETTER / SETTER *** */

    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password  = password_hash($password, PASSWORD_DEFAULT);
    }
    
}
