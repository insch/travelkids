<?php

namespace Controllers\Frontend;

use Entities\Travel;

//use Webmasters\Doctrine\ORM\Util;

class TravelController extends \Controllers\AbstractBase {

    public function indexAction() {
        $this->travellistAction();
    }

//list the travels 
    public function travellistAction() {
        $travels = $this->em->getRepository('Entities\Travel')->findAll();
        $this->addContext('travels', $travels);
        $this->setTemplate('travellistAction');
    }

//user and visitor detailed view of each travel
    public function traveldetailAction() {
        $id = filter_input(INPUT_GET, 'id');
        $travel = $this->em->getRepository('Entities\Travel')->find((int)$id);
        $travel || $this->render404();
        $this->addContext('travel', $travel);
        $this->setTemplate('traveldetailAction');
    }

    public function searchAction() {

    $form = filter_input_array(INPUT_POST);
    
    $query = $this->em->createQueryBuilder()
            ->select('t')
            ->from('\Entities\Travel', 't')
            ->leftJoin('t.category', 'c')
            ->leftJoin('t.region', 'r')
            ->where('t.title LIKE :search') //Prepared Statement :search als Schutz gegen SQL-Injection
            ->orWhere('t.teaser LIKE :search')
            ->orWhere('t.description LIKE :search')
            ->orWhere('c.title LIKE :search')
            ->orWhere('r.name LIKE :search')
            ->setParameter('search', '%' . $form['search'] . '%')
            ->getQuery();

    $searchResults = $query->getResult();

    $travels = $this->em->getRepository('Entities\Travel')->findAll();

    $this->addContext('travels', $travels);
    $this->addContext('searchInput', $form['search']);
    $this->addContext('searchResults', $searchResults);
    $this->setTemplate('searchAction');
    }
    
    public function searchIdAction() {
    $id = filter_input(INPUT_GET, 'id');
    $travel = $this->em->getRepository('Entities\Travel')->find((int)$id);
    $travel || $this->render404();
    
    $this->addContext('travel', $travel);
    $this->setTemplate('traveldetailAction');
}
}


