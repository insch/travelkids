<div class='content'>
    <h2>Liste aller Regionen</h2>
    <section class='regions'>
        <?php foreach ($regions as $region) { ?>
            <div>
                <a href='<?= BASE_DIR .'/index.php?controller=region&action=regionlist&id=' . $region->getId(); ?>'>
                    <img src='<?= BASE_DIR . '/img/pixel-regions/' . $region->getRegionimage(); ?>' alt='<?= $region->getName(); ?>'><span><?= $region->getName(); ?></span>
                </a>
            </div>
        <?php } ?>
    </section>
</div>
