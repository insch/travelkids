<div class='wrapperList'>
    <div class='content'>
        <main class='home'>
            <h1>Suchergebnis für Suchbegriff: "<?= clean($searchInput) ?>"</h1>
            <section class='top-reisen'>
                <?php if (count($searchResults)) { ?>
                    <?php foreach ($searchResults as $result) { ?>
                    <?php $travelstart = ($result->getTravelstart()->getDatetime()->getTimestamp()) ; 
                                $now = (new \DateTime())->getTimestamp();
                                if($travelstart > $now) { ?>
                        <article>
                                <a href='<?= BASE_DIR . '/index.php?controller=travel&action=traveldetail&id=' . $result->getId(); ?>' title=''><img src='<?= BASE_DIR .'/img/thumbnail/'. $result->getThumbnail(); ?>' alt=''><span>nur <?= $result->getPrice() . '€'; ?></span></a>
                                <h3 class='titel'><?= $result->getTitle() ?></h3>
                                <p>Reisedatum: <?= $result->getTravelstart()->format('d.m.Y'); ?> - <?= $result->getTravelend()->format('d.m.Y'); ?></p>
                                <p class='kurzBeschreibung'><?= $result->getTeaser(); ?><p>
                                    Reisekategorie:<a href='<?= BASE_DIR .'/index.php?controller=category&action=catlist&id=' . $result->getCategory()->getId(); ?>' title=''> <?= $result->getCategory()->getTitle(); ?></a><br>
                                    Region:<a href='<?= BASE_DIR . '/index.php?controller=region&action=regionlist&id=' . $result->getRegion()->getId(); ?>' title=''> <?= $result->getRegion()->getName(); ?></a><br>
                                    <br>
                                <a href='<?= BASE_DIR . '/index.php?controller=booking&action=form&id=' . $result->getId(); ?>'><button class="btns">Reise buchen! </button></a>
                        </article>
                <?php } } } else { ?>
                <p>Keine Reisen gefunden - schaut gern in <br>unserer <a href='<?= BASE_DIR .'/index.php?controller=travel&action=travellist'; ?>'>Reiseliste nach</a></p>
                <?php }?>   
            </section>
        </main>
    </div>
</div>
    