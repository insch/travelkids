<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Webmasters\Doctrine\ORM\Util;

/**
 * Doctrine\ORM\Mapping\Entity
 * @ORM\Entity
 * @ORM\Table(name="travels")
 */
class Travel extends AbstractEntity {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned" = true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $teaser;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $thumbnail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;
    
    /**
     * @ORM\Column(type="date")
     */
    private $travelstart;
    
    /**
     *
     * @ORM\Column(type="date")
     */
    private $travelend;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2, options={"unsigned" = true})
     */
    private $price;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="travels")
     */
    private $region;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="travels")
     */
    private $category;

    /**
     *
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="travel")
     */
    private $bookings;

    
    
    //festlegen, dass $bookings eine ArrayCollection ist
//    public function __construct() {
//        $this->bookings = new \Doctrine\Common\Collections\ArrayCollection(); //Zusatzmethoden aktivieren (contains, add, clear, remove) - ein Array mit Methoden
//    }

//Region hinzufügen mit ADD - da eine Reise mehrere Regionen haben kann
//    public function addRegion($region) {
//        //wenn eine bestimmte Region noch nicht vorhanden ist, dann diese Region einfügen
//        if (!$this->regions->contains($region)) {
//            $this->regions->add($region);
//        }
//    }
    
    // GETTER / SETTER
    
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getTeaser() {
        return $this->teaser;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getThumbnail() {
        return $this->thumbnail;
    }

    public function getImage() {
        return $this->image;
    }

    public function getTravelstart() {
        return new Util\DateTime($this->travelstart);
    }

    public function getTravelend() {
        return new Util\DateTime($this->travelend);
    }

    public function getPrice($comma = false) {
        if($comma) {
        return str_replace('.', ',', $this->price);
        } else {
            return $this->price;
        }
    }

    public function getRegion() {
        return $this->region;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getBookings() {
        return $this->bookings;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setTeaser($teaser) {
        $this->teaser = $teaser;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function setTravelstart($travelstart) {
        if(empty($travelstart)) {
            $travelstart = 'now';
        }
        $this->travelstart = new Util\DateTime($travelstart);
    }

    public function setTravelend($travelend) {
        if(empty($travelend)) {
            $travelend = 'now';
        }
        $this->travelend = new Util\DateTime($travelend);
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function setRegion($region) {
        $this->region = $region;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function setBookings($bookings) {
        $this->bookings = $bookings;
    }

}