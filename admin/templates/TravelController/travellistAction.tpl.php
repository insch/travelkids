<div class='content'>
    <h1>Unser gesamtes Reiseangebot</h1>
    <section class="reise-list">
        <?php foreach ($travels as $travel) {
            ?>
            <article>
                <h3>Reise-Nr: <?= ' ' . $travel->getId() . ' <br>Name: ' . $travel->getTitle(); ?></h3>

                <div>
                    <span>Reise-Datum: </span>
                    <span><?= $travel->getTravelstart()->format('d.m.Y') . '-' . $travel->getTravelend()->format('d.m.Y'); ?></span>
                </div>
                <div>
                    <span>Region: </span>
                    <span><?= $travel->getRegion()->getName(); ?></span>
                </div>
                <div>
                    <span>Kategorie: </span>
                    <span><?= $travel->getCategory()->getTitle(); ?></span>
                </div>
                <div>
                    <a href="<?= BASE_DIR . '/admin/index.php?controller=travel&action=edit&id=' . $travel->getId(); ?>" class="btns btn-xs">Reise anpassen</a>
                    <a href="<?= BASE_DIR . '/admin/index.php?controller=travel&action=delete&id=' . $travel->getId(); ?>" class="btns btn-xs">Reise löschen</a>
                </div>
            </article>
        <?php } ?>
    </section>
</div>

<script>
    setTimeout(() => {
        document.querySelector('.message').classList.add('fadeout');
    }, 3000);
</script>