<div class='wrapperStatic'>
    <div class='content'>
        <main id='static'>
            <h1>Ihr habt Fragen?</h1>
            <p>Ruft uns an <a href='tel:+4991112345678' class='telefon' onclick='hover true'><i class='fa fa-phone'></i> +49 (0)911 - 12 34 56 78</a>
                - oder schreiben Sie uns gern eine Nachricht:</p>
            <div class='contactForm'>

                <form action='<?= BASE_DIR .'/index.php?controller=index&action=kontakt.html'?>' method='post'>
                    <div id='message'>
                        <label for='frage'></label>
                        <textarea name='frage' id='frage' placeholder='Nachricht ...' rows='20'></textarea>
                    </div>
                    <div id='recipient'>
                        <!-- <div><sub>Postcard</sub></div> -->
                        <br/><img src='<?= BASE_DIR .'/img/briefmarke.png'?>' alt='briefmarke' class='briefmarke'/>

                        <label for='name'>Name:</label>
                        <input type='text' name='name' id='name' placeholder='...' required/>

                        <label for='email'>E-Mail:</label>
                        <input type='email' name='email' id='email' placeholder='...' required/>
                    </div>
<!--                    <input type='hidden' name='createdAt' id='createdAt'/>
                    <input type='hidden' name='mailto'  />-->
                    <button type='submit' class='btns kontaktBtn'>Anfrage senden</button>
                </form>
            </div>
        </main>
    </div>
</div>
