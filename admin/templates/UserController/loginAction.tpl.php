<div id='login'>
    <h1>Login für Travelkids Mitarbeiter</h1>
    <form class="loginform" action="index.php?controller=user&action=login" method="post">
        
        
        <label for="username">Benutzername</label>
        <input type="text" name="username" id='username' placeholder="Benutzername" />

        <label for="password">Passwort</label>
        <input type="password" name="password" id='password' placeholder="********" />
        
        <button type="submit" class="btns btn-s">Anmelden</button>
        
        <label for='stayLoggedIn'>
            <input type="checkbox" name="stayLoggedIn" id='stayLoggedIn'> Angemeldet bleiben
        </label>
        
        
        <?php if ($error) { ?>
            <div class="errormsg">
                <span>Die angegebenen Benutzerdaten sind nicht korrekt.</span>
            </div>
        <?php } ?>

    </form>

</div>
