<div id="logoAndNav">
    <div class="logo">
        <a href="<?= BASE_DIR ?>/index.php" title="Startseite"><img src="<?= BASE_DIR; ?>/img/logo/travelkids-logo-180x360.png" alt="Logo Travelkids.de"></a>
    </div>
    <nav id="nav"> <!--onclick="return true"-->
        <ul>
            <li class='nav-item start'><a href='<?= BASE_DIR; ?>/index.php'><i class="fa fa-suitcase"></i> Start</a></li>
            <li class='nav-item sea'><a href='<?= BASE_DIR; ?>/index.php?controller=category&action=catlist&id=1'><i class="fa fa-anchor" aria-hidden="true"></i> Ans Meer</a></li>
            <li class='nav-item park'><a href='<?= BASE_DIR; ?>/index.php?controller=category&action=catlist&id=2'><i class="fa fa-bug" aria-hidden="true"></i> Zoo &amp; Parks</a></li>
            <li class='nav-item nature'><a href='<?= BASE_DIR; ?>/index.php?controller=category&action=catlist&id=3'><i class="fa fa-tree" aria-hidden="true"></i> Ins Grüne</a></li>
            <li class='nav-item contact'><a href='<?= BASE_DIR; ?>/index.php?controller=index&action=kontakt'><i class="fa fa-envelope" aria-hidden="true"></i> Kontakt</a></li>
        </ul>
    </nav>
</div>
<div class='top-search'>
    <form action='index.php?controller=travel&action=search' method='post'>
        <input type='search' name='search' id='search'  list='travelSearch' placeholder='Eure Reise-Suche' autocomplete="off" value='<?php echo (isset($searchInput)) ? clean($searchInput) : ''; ?>'/>
        <datalist id="travelSearch">
            <?php
            if (isset($travels)) {
                foreach ($travels as $travel) { ?>
                <?php $travelstart = ($travel->getTravelstart()->getDatetime()->getTimestamp()) ; 
                            $now = (new \DateTime())->getTimestamp();
                            if($travelstart > $now) { ?>
                    <option value='<?= $travel->getTitle();?>'> </option>
                    <option value='<?= $travel->getCategory()->getTitle();?>'> </option>
                    <option value='<?= $travel->getRegion()->getName();?>'> </option>
            <?php }} } ?>
        </datalist>
        <button type="submit" disabled="disabled" name="searchBtn" id="searchBtn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
</div>