<div class='content'>
    <?php if (isset($travel)) { ?>
        <h2>Reise anpassen</h2>
    <?php } else { ?>
        <h2>Neue Reise anlegen</h2>
    <?php } ?>
    <hr>
    <br>

    <form action="<?= BASE_DIR; ?>/admin/index.php?controller=travel&action=form" method='post'>
        <?php if (isset($travel)) { ?>
            <input type='hidden' name='travelId' value='<?= $travel->getId(); ?>'/>
        <?php } ?>    
        <div class='row'>
            <label for='title'>Titel:</label>
            <input required type='text' name='title' id='title' placeholder='Titel der Reise' value='<?= (isset($travel)) ? $travel->getTitle() : ''; ?>' />
        </div>

        <div class='row'>
            <label for='teaser'>Kurze Beschreibung:</label>
            <textarea required id='teaser' name='teaser' placeholder='Beschreiben Sie die Reise in 1-3 kurzen Sätzen'><?= (isset($travel)) ? $travel->getTeaser() : ''; ?></textarea>
        </div>  

        <div class='row'>
            <label for='description'>Lange Beschreibung:</label>
            <textarea required id='description' name='description' placeholder='Beschreiben Sie die Reise ausführlich'><?= (isset($travel)) ? $travel->getDescription() : 'Beschreiben Sie die Reise ausführlich'; ?></textarea>
        </div>


        <div class='row'>
        <!--    <input type='text' name='detailbild' id='detailbild' value='Detailbild' />-->
        <!--    <input type='text' name='thumbnail' id='thumbnail' value='thumbnail' />-->
            <label for='image'>Detailbild:</label>
            <input required type='file' name='image' id='image' />
        </div>

        <div class='row'>
            <label for='thumbnail'>Vorschaubild:</label>
            <input required type='file' name='thumbnail' id='thumbnail'/>
        </div>

        <div class='row'>

            <label for='travelStart'>Beginn der Reise:</label>
            <input required type='date' name='travelStart' id='travelStart' value="<?= (isset($travel)) ? $travel->getTravelstart()->format('d.m.Y') : ''; ?>" />
        </div>

        <div class='row'>
            <label for='travelEnd'>Ende der Reise:</label>
            <input required type='date' name='travelEnd' id='travelEnd' value="<?= (isset($travel)) ? $travel->getTravelend()->format('d.m.Y') : ''; ?>" />
        </div>

        <div class='row'>
            <label for='price'>Preis in Euro:</label>
            <input required type='number' name='price' id='price' min='4.99' step='0.01' max='2500' value="<?= (isset($travel)) ? $travel->getPrice() : ''; ?>" />
        </div>

        <div class='row'>
            <label for='categoryId'>Eine Kategorie auswählen:</label>
            <div class='radio'>
                <?php foreach ($categories as $category) { ?>

                    <label><input required type='radio' name='categoryId' <?= (isset($travel) && ($travel->getCategory())) ? 'checked' : ''; ?> value="<?= $category->getId(); ?>" /> <?= $category->getTitle(); ?></label>

                <?php } ?> 
            </div>

        </div>

        <div class='row'>
            <label for='regionId'>Region:</label>

            <select required name='regionId'>
                <?php foreach ($regions as $region) { ?>
                    <option <?= (isset($travel) && ($travel->getRegion()->getName() == $region->getName())) ? 'selected' : ''; ?> value="<?= $region->getId(); ?>"> <?= $region->getName(); ?></option>
                <?php } ?>  
            </select>
        </div>

        <div class='row'>
            <?php if (isset($travel)) { ?>
                <button type='submit' class='btns btn-s'>Anpassen</button>
            <?php } else { ?>
                <input type='submit' class='btns btn-s' value='Hinzufügen' />
            <?php } ?>
                <a href='/reisebuero/admin/index.php'>Abbrechen</a>
        </div>
    </form>
</div>