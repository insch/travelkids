<div class='content'>
    <?php if (isset($category)) { ?>
        <h2>Kategorie anpassen</h2>
    <?php } else { ?>
        <h2>Neue Kategorie anlegen</h2>
    <?php } ?>

    <hr>
    <br>

    <form action="<?= BASE_DIR; ?>/admin/index.php?controller=category&action=save" method="post">

        <!-- EDIT fragen, ob die ID schon vorhanden/gesetzt ist -->
        <?php if (isset($category)) { ?>
            <input type="hidden" name="id" value="<?= $category->getId(); ?>" />
        <?php } ?>


        <div class="row">
            <label for="titel">Titel:</label>
            <input type="text" name="titel" id="titel" placeholder="Geben Sie den Titel der Kategorie ein" value="<?= (isset($category)) ? $category->getTitle() : ''; ?>" />

            <?php if (isset($category)) { ?>
                <button type="submit" class="btn">Anpassen</button>
            <?php } else { ?>
                <button type="submit" class="btn">Hinzufügen</button>
            <?php } ?>
        </div>

    </form>
</div>>