<?php

namespace Controllers\Frontend;

use Webmasters\Doctrine\ORM\Util;

class IndexController extends \Controllers\AbstractBase
{
    public function indexAction()
    {
        $query = $this->em->createQueryBuilder()
                ->select('t')
                ->from('Entities\Travel', 't')
                ->andwhere('t.travelstart >= :now')
                ->setParameter('now', (new \DateTime('now'))->format('Y-m-d H:i:s'))
                ->setMaxResults(6)
                ->getQuery();
        $travels = $query->getResult();
        
        $pool = array_values($travels);
        $sliderTravels = [];
        
        for($i = 0; $i < 5; $i++) { //max number of travels in the slider
            $rand = array_rand($pool);
            $randEntity = array_splice($pool, $rand, 1);
           
            $sliderTravels[] = $randEntity[0];
        }
        
        $regions = $this->em->getRepository('\Entities\Region')->findAll();
        $this->addContext('travels', $travels);
        $this->addContext('regions', $regions);
        $this->addContext('sliderTravels', $sliderTravels);
        $this->setTemplate('indexAction');
    }
     
    public function datenschutzAction() {
        $this->setTemplate('datenschutzAction');
    }

    public function impressumAction() {
        $this->setTemplate('impressumAction');
    }

    public function ueberunsAction() {
        $this->setTemplate('ueberunsAction');
    }
    
    public function kontaktAction() {
        $this->setTemplate('kontaktAction');
    }
}
