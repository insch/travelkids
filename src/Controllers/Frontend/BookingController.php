<?php

namespace Controllers\Frontend;

use Entities\Travel;
use Webmasters\Doctrine\ORM\Util;

class BookingController extends \Controllers\AbstractBase {

    public function indexAction() {
        $this->formAction();
    }
    
    public function formAction() {
        $id = filter_input(INPUT_GET, 'id');
        
        $region = $this->em->getRepository('Entities\Region')->find($id);

        $cat = $this->em->getRepository('Entities\Category')->find($id);
        
        $travel = $this->em->getRepository('Entities\Travel')->find((int)$id);
        $travel || $this->render404();
                
        $this->addContext('region', $region);
        $this->addContext('cat', $cat);
        $this->addContext('travel', $travel);
        $this->addContext('id', $id);
        $this->setTemplate('formAction');
    }

    public function saveAction() {
        
        $form = filter_input_array(INPUT_POST);
       
        $_SESSION['bookinginfo'] = $form;
        
        $travel = $this->em->getRepository('Entities\Travel')->find($form['travelId']);
        
        $this->addContext('travel', $travel);
    }
    
    public function dankeAction() {
      
        $form = $_SESSION['bookinginfo'];
        $currentBooking = $this->em->getRepository('Entities\Booking')->find($form['travelId']);
//        $currentBooking || $this->render404();
        
        if($_POST) {
            $booking = new \Entities\Booking();

            $booking->setByArray($form);

            $travel = $this->em->getRepository('Entities\Travel')->find($form['travelId']);
            
            $booking->setTravel($travel);
            $booking->setAgb(true);
            $booking->setBookingDate(new \DateTime());

            $this->em->persist($booking);
            $this->em->flush();
        }
 
        $this->addContext('travel', $travel);
        $this->addContext('currentBooking', $currentBooking);
        $this->addContext('booking', $booking);
       
        $this->setTemplate('dankeAction');        
    }
}