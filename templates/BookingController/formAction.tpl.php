<div class='wrapperStatic'>
    <div class='content'>
        <main id='static'>
            <button id='back-to-Btn' type='button' onclick='history.back()'>zurück</button>
            <h2>Kurz &amp; knapp - Reisezusammenfassung:</h2>
            <div class='kurzKnapp booking'>
                <div>
                    <h3><?= $travel->getTitle(); ?></h3>
                    <ul>
                        <i class='fa fa-calendar fa-2x' aria-hidden='true'></i><li>Reisezeit: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></li>
                        <i class='fa fa-map-marker fa-2x' aria-hidden='true'></i> <li> Region: <?= $travel->getRegion()->getName(); ?></li>
                        <i class='fa fa-users fa-2x' aria-hidden='true'></i> <li> max. Personen: 8</li>
                        <i class='fa fa-eur fa-2x' aria-hidden='true'></i> <li> Preis: <?= $travel->getPrice() . '€'; ?> pro Person</li>
                    </ul>
                </div>
                <div>
                    <img src="<?= BASE_DIR?>/img/thumbnail/<?= $travel->getThumbnail(); ?>" alt="" />
                    <p></p>
                    <p>Kurzbeschreibung: <?= $travel->getTeaser(); ?></p>
                </div>
            </div>

            <h2>Bucht jetzt eure Reise</h2>
            <div class='bookingForm'>
                <form action='<?= BASE_DIR;?>/index.php?controller=booking&action=save' method='post'>
                    
                    <input type='hidden' name='travelId' value='<?= clean($travel->getId()); ?>' />

                    <div class='row'>
                        <label for='salutation'>Anrede: </label>
                        <div>
                            <label class='radio' for='salutationHer'><input required type='radio' class='radio' name='salutation' id='salutationHer' value='Frau'/>Frau </label>
                            <label class='radio' for='salutationHim'><input required type='radio' class='radio' name='salutation' id='salutationHim' value='Herr'/>Herr </label>
                        </div>
                    </div>

                    <div class='row'>
                        <label for='firstname'>Vorname:</label>
                        <input required type='text' name='firstname' id='firstname' placeholder='Vorname' value='<?= (isset($bookings)) ? clean($bookings->getFirstname()) : ''; ?>'/>
                    </div>

                    <div class='row'>
                        <label for='lastname'>Nachname:</label>
                        <input required type='text' name='lastname' id='lastname' placeholder='Nachname' value='<?= (isset($bookings)) ? clean($bookings->getLastname()) : ''; ?>'/>
                    </div>

                    <div class='row'>
                        <label for='email'>E-Mail-Adresse:</label>
                        <input required type='email' name='email' id='email' placeholder='E-Mail-Adresse' value='<?= (isset($bookings)) ? clean($bookings->getEmail()) : ''; ?>'/>
                    </div>

                    <div class='row'>
                        <label for='birthday'>Geburtsdatum:</label>
                        <input required type='date' name='birthday' id='birthday' placeholder='tt.mm.jjjj' value='<?= (isset($bookings)) ? clean($bookings->getBirthday()) : ''; ?>'/>
                    </div>

                    <div class='row'>
                        <label for='phone'>Telefon-Nr.:</label>
                        <input required type='text' id='phone' name='phone' placeholder='Telefon-Nr.' value='<?= (isset($bookings)) ? clean($bookings->getPhone()) : ''; ?>'/>
                    </div>

                    <div class='row'>
                        <label for='street'>Straße:</label>
                        <input required type='text' name='street' id='street' placeholder='Straße' value='<?= (isset($bookings)) ? clean($bookings->getStreet()) : ''; ?>' />
                    </div>
                    <div class='row'>
                        <label for='streetnumber'>Hausnummer:</label>
                        <input required type='text' name='streetnumber' id='streetnumber' placeholder='212a' value='<?= (isset($bookings)) ? clean($bookings->getStreetnumber()) : ''; ?>'/>
                    </div>

                    <div class='row'>
                        <label for='zip'>PLZ:</label>
                        <input required type='text' name='zip' id='zip' placeholder='z.b. 90402' value='<?= (isset($bookings)) ? clean($bookings->getZip()) : ''; ?>' />
                    </div>

                    <div class='row'>
                        <label for='city'>Ort:</label>
                        <input required type='text' name='city' id='city' placeholder='Wohnort' value='<?= (isset($bookings)) ? clean($bookings->getCity()) : ''; ?>' />
                    </div>
                    <!-- Wie kann man die Eingabe des Datums erfassen? -->

                    <div class='row'>
                        <label for='persons'>Anzahl der Reisenden:</label>
                        <input required type='number' name='persons' id='persons' placeholder='Anzahl der Reisenden (1 - 8)' min='1' max='8' value='<?= (isset($bookings)) ? clean($bookings->getCity()) : ''; ?>'/>
                    </div>
                    
                    <div><input type="hidden" name="bookingdate" id="bookingdate" value="<?= (isset($bookings)) ? clean($bookings->getBookingDate()->format('Y-m-d')) : ''; ?>" /></div>

                    <div class='row'>
                        <button type='submit' class='btns'>Nächster Schritt: Eingaben prüfen</button>
                    </div>

                    <div class='row '><small>mit *) gekennzeichente Felder sind Pflichtangabe</small></div>
                </form>
            </div>

            <h3>Allgemeine Infos zur Buchung:</h3>
            <p>Lorem ipsum E-Mail mit Buchungsbestätigung sit amet, consectetur Reiseunterlagen elit. Harum laudantium ad doloremque nisi voluptatibus officiis deleniti, repellat sequi, rem mollitia repellendus obcaecati eius. Eligendi sint assumenda excepturi incidunt necessitatibus error saepe, amet officia porro eos, enim, minus distinctio nemo tenetur ad nihil dignissimos doloribus iusto? Repellat ad at, praesentium dolores debitis explicabo, modi sapiente numquam excepturi non reprehenderit enim? At excepturi ea culpa ratione unde, quos nihil, ex praesentium nemo esse sed, provident, quibusdam sunt. Inventore nobis labore veritatis autem quo odit reprehenderit, consectetur iure molestias beatae deserunt nemo quos rem eveniet. Asperiores omnis ab labore illum aperiam. Harum amet tempore quae veritatis ipsum laudantium perferendis, rem placeat ad magnam repudiandae perspiciatis accusamus consequatur quisquam voluptatem eum assumenda, laborum quo aperiam debitis quaerat. Dignissimos cumque necessitatibus harum assumenda quisquam in temporibus earum quia reprehenderit alias atque, debitis, porro facilis illo veniam quae facere neque nobis omnis consequuntur ipsa vero laboriosam.</p>

        </main>
    </div>
</div>