<div class='content'>
    <h3> Wollen Sie die Reise " <?php echo clean($travel->getTitle()) ?> " wirklich unwiderruflich löschen?</h3>
    
    <form action='<?= BASE_DIR; ?>/admin/index.php?controller=travel&action=delete' method='post'>
        <input name='id' type='hidden' value='<?php echo clean($travel->getId()); ?>'/>
               <button type='submit' class='btns btn-l'>Ja, Reise löschen</button>
               <a href='<?= BASE_DIR; ?>/admin/index.php'>Abbrechen</a>
    </form>
</div>