<?php

namespace Controllers\Backend;

class IndexController extends \Controllers\AbstractBase
{
    public function indexAction()
    {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
        $this->redirect('travellist', 'travel');
        } else {
        $this->redirect('login', 'user');            
        }
    }
}
