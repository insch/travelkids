<?php

namespace Validators;

use Webmasters\Doctrine\ORM\EntityValidator;
use Webmasters\Doctrine\ORM\Util;


class TravelValidator extends EntityValidator {
    
    public function validateTitle($title) {
        if(empty($title)) {
            $this->addError('Das Feld Reisetitel ist leer.');
        } elseif (strlen($title) < 3) {
            $this->addError('Der Reisetitel sollte mindestens 3 Zeichen lang sein.');
        }
    }
    
    public function validateTeaser($teaser) {
        if(empty($teaser)) {
            $this->addError('Das Feld Kurzbeschreibung ist leer.');
        } elseif (strlen($teaser) < 60) {
            $this->addError('Die Kurzbeschreibung sollte mindestens 60 Zeichen lang sein.');
        }
    }
    
    public function validateDescription($description) {
        if(empty($description)) {
            $this->addError('Das Feld Beschreibung ist leer.');
        } elseif (strlen($description) < 400) {
            $this->addError('Die Beschreibung sollte mindestens 400 Zeichen lang sein.');
        }
    }
    
    public function validateImage($image) {
        if(empty($image)) {
            $this->addError('Es fehlt das Detailbild. Bitte laden Sie ein Bild hoch');
        }
    }
    
    public function validateThumbnail($thumbnail) {
        if(empty($thumbnail)) {
            $this->addError('Es fehlt das Detailbild. Bitte laden Sie ein Bild hoch');
        }
    }
    
    public function validatePrice($price) {
        if(empty($price)) {
            $this->addError('Das Feld Preis ist leer.');
        } elseif (strlen($price) < 5) {
            $this->addError('Der Preis sollte mindestens 10,00 Euro betragen.');
        } elseif (strlen($price) > 7) {
            $this->addError('Der Preis sollte maximal 9999,99 Euro betragen.');
        }
    }
    
    public function validateTravelstart($travelstart) {
        $now = new Util\DateTime('now');
        
        if(!$travelstart->isValid()) {
            $this->addError('Das Feld Startdatum muss einen korrekten Datumswert enthalten.');
        }  elseif (!$now->isValidClosingDate($travelstart)) {
            $this->addError('Das Startdatum darf nicht in der Vergangenheit liegen.');
        }
    }
    
    public function validateTravelend($travelend) {
        $now = new Util\DateTime('now');
        
        if(empty($travelend)) {
            $this->addError('Das Feld Enddatum der Reise ist leer');
        } elseif (!$now->isValidClosingDate($travelend)) {
            $this->addError('Das Enddatum darf nicht in der Vergangenheit liegen.');
        }
    }
    
    public function validateRegion($region) {
        if(empty($region)) {
            $this->addError('Die Region wurde nicht ausgewählt.');
        }
    }
    
    public function validateCategory($category) {
        if(empty($category)) {
            $this->addError('Die Kategorie wurde nicht ausgewählt.');
        }
    }
}