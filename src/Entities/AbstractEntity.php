<?php

//namespace Controllers;
namespace Entities;

/**
 * Description of AbstractEntity
 * @author TM1
 */
abstract class AbstractEntity {
    function setByArray(array $arr){
        if($arr){
            foreach ($arr as $k => $v){
                $setter = 'set'.ucfirst($k);
                if(method_exists($this, $setter)){
                    $this->$setter($v);
                }
            }
        }
    }
}
