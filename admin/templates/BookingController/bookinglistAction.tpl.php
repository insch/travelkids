<div class='content'>
    <h1>Übersicht aller Buchungen</h1>
    <div class='cats'>
        <p>sortiert nach Buchungsdatum</p>
        <?php
        foreach ($bookings as $booking) {
            ?>
            <div id='bookings'>
                <h3>Buchung-Nr: <?= ' ' . $booking->getId() . ' gebucht am: ' . $booking->getBookingDate()->format('d.m.Y - H:i'); ?></h3>

                <table>
                    <tbody>
                        <tr>
                            <td>Reisetitel: </td>
                            <td><?= $booking->getTravel()->getTitle(); ?></td>
                        </tr>
                        <tr>
                            <td>Reise-Datum: </td>
                            <td><?= $booking->getTravel()->getTravelstart()->format('d.m.Y') . '-' . $booking->getTravel()->getTravelend()->format('d.m.Y'); ?></td>
                        </tr>
                        <tr>
                            <td>Personenanzahl: </td>
                            <td><?= $booking->getPersons(); ?></td>
                        </tr>
                        <tr>
                            <td>E-Mail-Adresse: </td>
                            <td><a href="mailto: <?= $booking->getEmail(); ?>"><?= $booking->getEmail(); ?></a></td>
                        </tr>
                        <tr>
                            <td>Kunde: </td>
                            <td><?= $booking->getFirstName() . ' ' . $booking->getLastName(); ?></td>
                        </tr>
                        <tr>
                            <td>Anschrift: </td>
                            <td><?= $booking->getStreet() . ' ' . $booking->getStreetnumber() . ', ' . $booking->getZip() . ' ' . $booking->getCity(); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>
</div>