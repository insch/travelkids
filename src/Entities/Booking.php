<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Webmasters\Doctrine\ORM\Util;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Doctrine\ORM\Mapping\Entity
 * @ORM\Entity
 * @ORM\Table(name="bookings")
 */
class Booking extends AbstractEntity {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned" = true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $salutation;

    /**
     * @ORM\Column(name="firstname", type="string", length=150)
     */
    private $firstName;

    /**
     * @ORM\Column(name="lastname", type="string", length=150)
     */
    private $lastName;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $streetnumber;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $city;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $agb;

    /**
     * @ORM\Column(name="bookingdate", type="datetime")
     */
    private $bookingDate;

    /**
     * @ORM\Column(type="integer", options={"unsigned" = true})
     */
    private $persons;

    /**
     * @ORM\ManyToOne(targetEntity="Travel", inversedBy="bookings")
     */
    private $travel;
    
    /* *** CONSTRUCTOR *** */
    public function __construct() {
        $this->birthday = new \DateTime();
    }
    
    /* GETTER / SETTER */

    public function getId() {
        return $this->id;
    }

    public function getSalutation() {
        return $this->salutation;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getBirthday() {
        return new Util\DateTime($this->birthday);
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getStreet() {
        return $this->street;
    }

    public function getStreetnumber() {
        return $this->streetnumber;
    }

    public function getZip() {
        return $this->zip;
    }

    public function getCity() {
        return $this->city;
    }
    
    public function getAgb() {
        return $this->agb;
    }

    public function getBookingDate() {
        return new Util\DateTime($this->bookingDate);
    }

    public function getPersons() {
        return $this->persons;
    }

    public function getTravel() {
        return $this->travel;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setSalutation($salutation) {
        $this->salutation = $salutation;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setBirthday($birthday) {
        $this->birthday = new Util\DateTime($birthday);
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setStreet($street) {
        $this->street = $street;
    }

    public function setStreetnumber($streetnumber) {
        $this->streetnumber = $streetnumber;
    }

    public function setZip($zip) {
        $this->zip = $zip;
    }

    public function setCity($city) {
        $this->city = $city;
    }
    
    public function setAgb($agb) {
        $this->agb = $agb;
    }

    public function setBookingDate($bookingDate) {
        $this->bookingDate = new Util\DateTime($bookingDate);
    }

    public function setPersons($persons) {
        $this->persons = $persons;
    }

    public function setTravel($travel) {
        $this->travel = $travel;
    }
    
}
