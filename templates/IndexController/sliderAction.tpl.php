<div class='arrowBtn btnPrev'>❮</div>
<div class='arrowBtn btnNext'>❯</div>
<div class='slider no-gap'>
<?php foreach ($sliderTravels as $travel) { ?>
    
        <div id='moveIt'>
        <img class='sliderImg' src='<?= BASE_DIR . '/img/slider/' . $travel->getImage(); ?>' alt=''>
        <div class='teaser teaserHome sliderBox'>
        <h3 class='titel'><?= $travel->getTitle(); ?></h3>
        <?php if (strlen($travel->getTeaser()) > 100) { ?>
        <p class='kurzBeschreibung'><?=
        substr($travel->getTeaser(), 0, 100) . '... <a href="index.php?controller=travel&action=traveldetail&id=' . $travel->getId() . '">mehr</a>';
        } else {?>
        <p class='kurzBeschreibung'><?= $travel->getTeaser() . '... <a href="index.php?controller=travel&action=traveldetail&id=' . $travel->getId() . '">mehr</a>'; ?></p>
        <?php } ?>
        <p class='teaserPreis'><span class='preisProPerson'><?= $travel->getPrice() . '€'; ?></span><br><span class='teaserPreisEdit'>pro Person </span></p>
        <p>vom <span class='reiseStart'><?= $travel->getTravelstart()->format('d.m.Y'); ?></span> bis <span class='reiseEnde'><?= $travel->getTravelend()->format('d.m.Y'); ?></span>
        <a href='<?= BASE_DIR . '/index.php?controller=booking&action=form&id=' . $travel->getId() ?>'><button type='button' class='btns teaserBtn'>Reise buchen</button></a>
        </div>
        </div>
<?php } ?>
</div>
<div id='point-nav'></div>