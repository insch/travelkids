<div class="wrapperStatic">
    <div class='content'>
        <main id='static'>
            <h2>Das sind wir - das Reisebüro hinter Travelkids</h2>
            <div class='aboutus'>
                <div class='profile'>
                    <img src='<?= BASE_DIR .'/img/detail/monika.jpg'?>' alt=''>
                    <div>
                        <h3>Monika Gerke</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam fugit vitae debitis voluptates distinctio, tempore deleniti id maiores illum vero ipsa sit aperiam cupiditate eum, adipisci atque? Qui, rerum, porro! Laboriosam illum natus ex, quia voluptates fugit deserunt labore autem.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero dolor animi ea, repudiandae nobis iste eligendi. Et quas asperiores, facere! Facilis quis distinctio cumque. In officia saepe est obcaecati nisi!</p>
                    </div>
                </div>
                <div class='profile'>
                    <img src='<?= BASE_DIR .'/img/detail/petra.jpg' ?>' alt=''>
                    <div>
                        <h3>Petra Falter</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque in voluptatum autem! Rerum et minima, veritatis laudantium reiciendis tempore quam itaque nostrum eaque tenetur sunt soluta, dolore vero voluptatum inventore ipsam vel aliquam at, magnam corrupti quae dicta ab. Ad, labore. Ratione dignissimos nulla odit incidunt architecto animi laborum dolorum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe voluptatibus natus quia iusto laudantium voluptatum, unde non corporis ullam id?</p>
                    </div>
                </div>
            </div>

            <h3>Sie haben Fragen?</h3>
            <p>Rufen Sie uns an <a href='tel:+4991112345678' class='telefon' onclick='hover true'><i class='fa fa-phone'></i> +49 (0)911 - 12 34 56 78</a>
                - oder schreiben Sie uns gern eine <a href='<?= BASE_DIR . '/index.php?controller=index&action=kontakt'; ?>'>Nachricht</a></p>
        </main>
    </div>
</div>