<div class='wrapperList'>
    <div class='content'>
        <main class='home'>
            <h1>Alle Travelkids - Reisen im Überblick</h1>
                <section class='top-reisen'>
                    <?php
                    if (count($travels)) {
                        foreach ($travels as $travel) { ?>
                            <?php $travelstart = ($travel->getTravelstart()->getDatetime()->getTimestamp()) ; 
                            $now = (new \DateTime())->getTimestamp();
                            if($travelstart > $now) { ?>
                            <article>
                                <a href='index.php?controller=travel&action=traveldetail&id=<?= $travel->getId(); ?>' title=''><img src='<?= BASE_DIR; ?>/img/thumbnail/<?= $travel->getThumbnail(); ?>' alt=''><span>nur <?= $travel->getPrice() . '€'; ?></span></a>
                                <h3 class='titel'><?= $travel->getTitle() ?></h3>
                                <p>Reisedatum: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></p>
                                <p class='kurzBeschreibung'><?= $travel->getTeaser(); ?><p>
                                <a href='<?= BASE_DIR ?>/index.php?controller=booking&action=form&id=<?= $travel->getId() ?>'><button class='btns btn-m'>Reise buchen</button></a>
                            </article>
                            <?php } } ?>
                </section>
            </main> 
<?php } ?>
    </div> 
</div>