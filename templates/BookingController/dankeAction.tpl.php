<?php //echo '<pre>'; 
            //var_dump($_SESSION['bookinginfo']);
            //echo '</pre>';?>
           

<div class='wrapperStatic'>
    <h2>Vielen Dank für Eure Buchung</h2>


    <div class='kurzKnapp booking'>
        <div>
            <h3>Eure Buchungsdetails: <?= 'TK-R2017 - ' . $booking->getId(); ?></h3>

            <p><span>Anrede: </span><?= $booking->getSalutation(); ?></p>
            <p><span>Name: </span><?= $booking->getFirstname() . ' ' . $booking->getLastname(); ?></p>
            <p><span>Geburtsdatum: </span><?= $booking->getBirthday()->format('d.m.Y'); ?></p>
            <p><span>E-Mail: </span><?= $booking->getEmail(); ?></p>
            <p><span>Telefon: </span><?= $booking->getPhone(); ?></p>
            <p><span>Straße: </span><?= $booking->getStreet() . ' ' . $booking->getStreetnumber(); ?></p>
            <p><span>Ort: </span><?= $booking->getZip() . ' ' . $booking->getCity(); ?></p>
            <p><span>Anzahl der Reisenden: </span><?= $booking->getPersons(); ?></p>
            <p><span>Gesamtpreis: </span><?= ($travel->getPrice() * $booking->getPersons()) . ' €'; ?></p>
        </div>
        <div>
            <h3>Eure Reisedaten:</h3>
            <p><span>Gebuchte Reise: </span><?= $travel->getTitle(); ?></p>
            <p><span>Reisedatum: </span><?= $travel->getTravelstart()->format('d.m.Y') . ' - ' . $travel->getTravelend()->format('d.m.Y'); ?></p>
            <br>
        </div>
                
    </div>
    <p>Ihr erhaltet in Kürze eine E-Mail mit all euren Reiseunterlagen sowie der Buchungsbestätigung. In den meisten Fällen reicht es aus, wenn ihr diese Unterlagen digital vorlegt.</p>
    <p>Ruft uns an <a href='tel:+4991112345678' class='telefon' onclick='hover true'><i class='fa fa-phone'></i> +49 (0)911 - 12 34 56 78</a></p>
</div>

<script>
    </script>