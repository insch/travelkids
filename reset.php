<?php

require_once 'inc/bootstrap.inc.php';

$em->getConnection()->query('SET FOREIGN_KEY_CHECKS=0');
$em->getConnection()->query('TRUNCATE TABLE users;');
$em->getConnection()->query('TRUNCATE TABLE travels;');
$em->getConnection()->query('TRUNCATE TABLE categories;');
$em->getConnection()->query('TRUNCATE TABLE regions;');
$em->getConnection()->query('TRUNCATE TABLE bookings;');
$em->getConnection()->query('SET FOREIGN_KEY_CHECKS=1');


/* * ** USER *** */

$users = [
    ['username' => 'Admin', 'password' => 'root'],
    ['username' => 'Inga', 'password' => 'Inga2017!'],
    ['username' => 'Max', 'password' => 'Max/1988'],
];

foreach ($users as $input) {
    $user = new Entities\User();
    $user->setByArray($input);
    $em->persist($user);
}
$em->flush();


/* * ** CATEGORY *** */
$cats = [
    ['title' => 'Wasser'],
    ['title' => 'Parks'],
    ['title' => 'Natur'],
];

foreach ($cats as $input) {
    $category = new Entities\Category();
    $category->setByArray($input);

    $em->persist($category);
}
$em->flush();

/* * ** CATEGORY END *** */

/* * ** REGIONS *** */
$regions = [
    ['name' => 'Schleswig-Holstein', 'regionimage' => 'schleswig-holstein.jpg'],
    ['name' => 'Mecklenburg-Vorpommern', 'regionimage' => 'mecklenburg-vorpommern.jpg'],
    ['name' => 'Hamburg', 'regionimage' => 'hamburg.jpg'],
    ['name' => 'Bremen', 'regionimage' => 'bremen.jpg'],
    ['name' => 'Niedersachsen', 'regionimage' => 'niedersachsen.jpg'],
    ['name' => 'Brandenburg', 'regionimage' => 'brandenburg.jpg'],
    ['name' => 'Berlin', 'regionimage' => 'berlin.jpg'],
    ['name' => 'Sachsen-Anhalt', 'regionimage' => 'sachsen-anhalt.jpg'],
    ['name' => 'Nordrhein-Westfalen', 'regionimage' => 'nordrhein-westfalen.jpg'],
    ['name' => 'Sachsen', 'regionimage' => 'sachsen.jpg'],
    ['name' => 'Hessen', 'regionimage' => 'hessen.jpg'],
    ['name' => 'Saarland', 'regionimage' => 'saarland.jpg'],
    ['name' => 'Rheinland-Pfalz', 'regionimage' => 'rheinland-pfalz.jpg'],
    ['name' => 'Thüringen', 'regionimage' => 'thueringen.jpg'],
    ['name' => 'Bayern', 'regionimage' => 'bayern.jpg'],
    ['name' => 'Baden-Württemberg', 'regionimage' => 'baden-wuerttemberg.jpg'],
];

foreach ($regions as $input) {
    $region = new Entities\Region();
    $region->setByArray($input);

    $em->persist($region);
}

$em->flush();
/* * ** REGIONS END *** */


/* * ** TRAVELS ** */
$travels = [
    [
        'title' => 'Angel Abenteuer',
        'teaser' => 'Cum sociis natoque penatibus et magnis dis parturient montes.',
        'description' => '<h1>Angel Abenteuer - für ruhige Abenteurer</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum sint itaque, possimus,
                        nesciunt placeat dignissimos animi eligendi et asperiores aliquid voluptatum suscipit
                        est repudiandae similique maxime, mollitia molestias maiores quaerat id!
                    </p>

                    <h3>Die Ausstattung eurer Unterkunft</h3>
                    <ul>
                        <li>kostenloses WLAN</li>
                        <li>TV, SAT id in suscipit quas excepturi, rem modi,</li>
                        <li>Vollbad odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Hochstuhl, Kinderbett dolorem eligendi voluptatibus,</li>
                        <li>Dreibett- oder Familienzimmer molestiae quam</li>
                    </ul>

                    <h3>Ihre Reisedaten: <span class="reiseStart">07.04.2018</span> bis <span class="reiseEnde">14.07.2018</span></h3>

                    <div id="booking-mobile">
                        <p>214,99 € <span>pro Person</span></p>
                        <a href="<?= BASE_DIR;?>/index.php?controller=booking&action=form&id=<?= $travel->getId(); ?>"><button class="detailBtn">Reise buchen</button></a>
                    </div>

                    <h3>In dieser Zeit finden folgende Events statt</h3>
                    <ul>
                        <li>Frühlingsfest: 08.04.2018 Strand Lorem ipsum dolor sit amet.</li>
                        <li>Wattparade: 10.04.2018 odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Pony-Sonntag: 11.04.2018 Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    </ul>

                    <h3>Entfernungen zu spannenden Orten in der Umgebung</h3>
                    <ul>
                        <li>Strand: Hic sapiente ad perferendis aspernatur</li>
                        <li>Seerobben-Aufnahme: id in suscipit quas excepturi, rem modi,</li>

                    </ul>
                    molestiae quam ut doloribus
                        asperiores iure debitis facere voluptatum incidunt sequi. Explicabo nemo dignissimos suscipit
                        deserunt molestiae. Unde illum quasi nisi perspiciatis provident aut molestias eius eum tempore
                        culpa! Pariatur iste quia repellendus dolores commodi animi, consequatur ad praesentium incidunt,
                        ipsum nobis reiciendis voluptatibus necessitatibus.</p>',
        'thumbnail' => 'angeln-thumbnail.jpg',
        'image' => 'angeln2560.jpg',
        'travelstart' => new DateTime('2018-04-07'),
        'travelend' => new DateTime('2018-04-14'),
        'price' => 214.99,
        'cat' => 1,
        'region' => 1],
    [
        'title' => 'Serengeti Park',
        'teaser' => 'Im Herzen Niedersachsens, am Rande der Lüneburger Heide, liegt Europas größter und einzigartiger Safaripark. Unser als Zoologischer Garten anerkannter Park bietet über 1500 freilaufenden Wildtieren ein naturnahes Zuhause.',
        'description' => '<h1>Familienzeit im Serengeti Park</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum sint itaque, possimus,
                        nesciunt placeat dignissimos animi eligendi et asperiores aliquid voluptatum suscipit
                        est repudiandae similique maxime, mollitia molestias maiores quaerat id!
                    </p>

                    <h3>Die Ausstattung eurer Unterkunft</h3>
                    <ul>
                        <li>kostenloses WLAN</li>
                        <li>TV, SAT id in suscipit quas excepturi, rem modi,</li>
                        <li>Vollbad odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Hochstuhl, Kinderbett dolorem eligendi voluptatibus,</li>
                        <li>Dreibett- oder Familienzimmer molestiae quam</li>
                    </ul>

                    <h3>Ihre Reisedaten: <span class="reiseStart">08.10.2018</span> bis <span class="reiseEnde">09.10.2018</span></h3>

                    <div id="booking-mobile">
                        <p>49,99 € <span>pro Person</span></p>
                        <a href="<?= BASE_DIR;?>/index.php?controller=booking&action=form&id=<?= $travel->getId(); ?>"><button class="detailBtn">Reise buchen</button></a>
                    </div>

                    <h3>In dieser Zeit finden folgende Events statt</h3>
                    <ul>
                        <li>Löwenfütterung: 09.10.2018 Strand Lorem ipsum dolor sit amet.</li>
                        <li>Sergenti-Safari: 10.10.2018 odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Pony-Ausritt: 10.10.2018 Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    </ul>
                    <h3>Entfernungen zu spannenden Orten in der Umgebung</h3>
                    <ul>
                        <li>Strand: Hic sapiente ad perferendis aspernatur</li>
                        <li>Seerobben-Aufnahme: id in suscipit quas excepturi, rem modi,</li>

                    </ul>
                    molestiae quam ut doloribus
                        asperiores iure debitis facere voluptatum incidunt sequi. Explicabo nemo dignissimos suscipit
                        deserunt molestiae. Unde illum quasi nisi perspiciatis provident aut molestias eius eum tempore
                        culpa! Pariatur iste quia repellendus dolores commodi animi, consequatur ad praesentium incidunt,
                        ipsum nobis reiciendis voluptatibus necessitatibus.</p>',
        'thumbnail' => 'loewe-thumbnail.jpg',
        'image' => 'loewe2560.jpg',
        'travelstart' => new DateTime('2018-10-09'),
        'travelend' => new DateTime('2018-10-10'),
        'price' => 39.99,
        'cat' => 2,
        'region' => 5],
    [
        'title' => 'Urlaub in einer Mühle',
        'teaser' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        'description' => '<h1>Urlaub in einer Mühle </h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum sint itaque, possimus,
                        nesciunt placeat dignissimos animi eligendi et asperiores aliquid voluptatum suscipit
                        est repudiandae similique maxime, mollitia molestias maiores quaerat id!
                    </p>

                    <h3>Die Ausstattung eurer Unterkunft</h3>
                    <ul>
                        <li>kostenloses WLAN</li>
                        <li>TV, SAT id in suscipit quas excepturi, rem modi,</li>
                        <li>Vollbad odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Hochstuhl, Kinderbett dolorem eligendi voluptatibus,</li>
                        <li>Dreibett- oder Familienzimmer molestiae quam</li>
                    </ul>

                    <h3>Ihre Reisedaten: <span class="reiseStart">09.09.2018</span> bis <span class="reiseEnde">16.09.2018</span></h3>

                    <div id="booking-mobile">
                        <p>299,99 € <span>pro Person</span></p>
                        <a href="<?= BASE_DIR;?>/index.php?controller=booking&action=form&id=<?= $travel->getId(); ?>"><button class="detailBtn">Reise buchen</button></a>
                    </div>

                    <h3>In dieser Zeit finden folgende Events statt</h3>
                    <ul>
                        <li>Drachenfest: 13.09.2018 Strand Lorem ipsum dolor sit amet.</li>
                        <li>Wattparade: 14.09.2018 odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Pony-Sonntag: 12.09.2018 Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    </ul>

                    <h3>Entfernungen zu spannenden Orten in der Umgebung</h3>
                    <ul>
                        <li>Strand: Hic sapiente ad perferendis aspernatur</li>
                        <li>Seerobben-Aufnahme: id in suscipit quas excepturi, rem modi,</li>

                    </ul>
                    molestiae quam ut doloribus
                        asperiores iure debitis facere voluptatum incidunt sequi. Explicabo nemo dignissimos suscipit
                        deserunt molestiae. Unde illum quasi nisi perspiciatis provident aut molestias eius eum tempore
                        culpa! Pariatur iste quia repellendus dolores commodi animi, consequatur ad praesentium incidunt,
                        ipsum nobis reiciendis voluptatibus necessitatibus.</p>',
        'thumbnail' => 'muehle-thumbnail.jpg',
        'image' => 'muehle2560.jpg',
        'travelstart' => new DateTime('2018-09-09'),
        'travelend' => new DateTime('2018-09-16'),
        'price' => 399.99,
        'cat' => 3,
        'region' => 2],
    [
        'title' => 'Berg und Tal',
        'teaser' => 'Penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        'description' => '<h1>Berg und Tal - Wanderurlaub mit Groß und Klein</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum sint itaque, possimus,
                        nesciunt placeat dignissimos animi eligendi et asperiores aliquid voluptatum suscipit
                        est repudiandae similique maxime, mollitia molestias maiores quaerat id!
                    </p>

                    <h3>Die Ausstattung eurer Unterkunft</h3>
                    <ul>
                        <li>kostenloses WLAN</li>
                        <li>TV, SAT id in suscipit quas excepturi, rem modi,</li>
                        <li>Vollbad odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Hochstuhl, Kinderbett dolorem eligendi voluptatibus,</li>
                        <li>Dreibett- oder Familienzimmer molestiae quam</li>
                    </ul>

                    <h3>Ihre Reisedaten: <span class="reiseStart">29.09.2018</span> bis <span class="reiseEnde">03.10.2018</span></h3>

                    <div id="booking-mobile">
                        <p>289,99 € <span>pro Person</span></p>
                        <a href="<?= BASE_DIR;?>/index.php?controller=booking&action=form&id=<?= $travel->getId(); ?>"><button class="detailBtn">Reise buchen</button></a>
                    </div>

                    <h3>In dieser Zeit finden folgende Events statt</h3>
                    <ul>
                        <li>Drachenfest: 03.10.2018 Strand Lorem ipsum dolor sit amet.</li>
                        <li>Brockenlauf: 30.09.2018 odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Pony-Sonntag: 01.10.2018 Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    </ul>

                    <h3>Entfernungen zu spannenden Orten in der Umgebung</h3>
                    <ul>
                        <li>Strand: Hic sapiente ad perferendis aspernatur</li>
                        <li>Seerobben-Aufnahme: id in suscipit quas excepturi, rem modi,</li>

                    </ul>
                    molestiae quam ut doloribus
                        asperiores iure debitis facere voluptatum incidunt sequi. Explicabo nemo dignissimos suscipit
                        deserunt molestiae. Unde illum quasi nisi perspiciatis provident aut molestias eius eum tempore
                        culpa! Pariatur iste quia repellendus dolores commodi animi, consequatur ad praesentium incidunt,
                        ipsum nobis reiciendis voluptatibus necessitatibus.</p>',
        'thumbnail' => 'berge-thumbnail.jpg',
        'image' => 'berge2560.jpg',
        'travelstart' => new DateTime('2018-09-29'),
        'travelend' => new DateTime('2018-10-03'),
        'price' => 289.99,
        'cat' => 3,
        'region' => 7],
    [
        'title' => 'Bauernhof-Ferien',
        'teaser' => 'Natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        'description' => '<h1>Bauernhof-Ferien</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum sint itaque, possimus,
                        nesciunt placeat dignissimos animi eligendi et asperiores aliquid voluptatum suscipit
                        est repudiandae similique maxime, mollitia molestias maiores quaerat id!
                    </p>

                    <h3>Die Ausstattung eurer Unterkunft</h3>
                    <ul>
                        <li>kostenloses WLAN</li>
                        <li>TV, SAT id in suscipit quas excepturi, rem modi,</li>
                        <li>Vollbad odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Hochstuhl, Kinderbett dolorem eligendi voluptatibus,</li>
                        <li>Dreibett- oder Familienzimmer molestiae quam</li>
                    </ul>

                    <h3>Ihre Reisedaten: <span class="reiseStart">09.10.2018</span> bis <span class="reiseEnde">23.10.2018</span></h3>

                    <div id="booking-mobile">
                        <p>529,99 € <span>pro Person</span></p>
                        <a href="<?= BASE_DIR;?>/index.php?controller=booking&action=form&id=<?= $travel->getId(); ?>"><button class="detailBtn">Reise buchen</button></a>
                    </div>

                    <h3>In dieser Zeit finden folgende Events statt</h3>
                    <ul>
                        <li>Drachenfest: 10.10.2018 Strand Lorem ipsum dolor sit amet.</li>
                        <li>Wattparade: 14.10.2018 odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Pony-Sonntag: 17.10.2018 Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    </ul>

                    <div id="dscBild">
                        <img src="<?= BASE_DIR;?>/img/thumbnail/selfie-natur.jpg" alt="Toll hier"><img src="<?= BASE_DIR;?>/img/thumbnail/schaf-thumbnail.jpg">
                    </div>

                    <h3>Entfernungen zu spannenden Orten in der Umgebung</h3>
                    <ul>
                        <li>Strand: Hic sapiente ad perferendis aspernatur</li>
                        <li>Seerobben-Aufnahme: id in suscipit quas excepturi, rem modi,</li>

                    </ul>
                    molestiae quam ut doloribus
                        asperiores iure debitis facere voluptatum incidunt sequi. Explicabo nemo dignissimos suscipit
                        deserunt molestiae. Unde illum quasi nisi perspiciatis provident aut molestias eius eum tempore
                        culpa! Pariatur iste quia repellendus dolores commodi animi, consequatur ad praesentium incidunt,
                        ipsum nobis reiciendis voluptatibus necessitatibus.</p>',
        'thumbnail' => 'bauernhof-thumbnail.jpg',
        'image' => 'bauernhof2560.jpg',
        'travelstart' => new DateTime('2018-10-09'),
        'travelend' => new DateTime('2018-10-23'),
        'price' => 429.99,
        'cat' => 1,
        'region' => 6],
    [
        'title' => 'Sorglos im Ferienhaus',
        'teaser' => 'Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        'description' => '<h1> Planschen, toben, genießen - im eigenen Ferienhaus</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum sint itaque, possimus,
                        nesciunt placeat dignissimos animi eligendi et asperiores aliquid voluptatum suscipit
                        est repudiandae similique maxime, mollitia molestias maiores quaerat id!
                    </p>

                    <h3>Die Ausstattung eurer Unterkunft</h3>
                    <ul>
                        <li>kostenloses WLAN</li>
                        <li>TV, SAT id in suscipit quas excepturi, rem modi,</li>
                        <li>Vollbad odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Hochstuhl, Kinderbett dolorem eligendi voluptatibus,</li>
                        <li>Dreibett- oder Familienzimmer molestiae quam</li>
                    </ul>

                    <h3>Ihre Reisedaten: <span class="reiseStart">04.11.2018</span> bis <span class="reiseEnde">11.11.2018</span></h3>

                    <div id="booking-mobile">
                        <p>419,99 € <span>pro Person</span></p>
                        <a href="<?= BASE_DIR;?>/index.php?controller=booking&action=form&id=<?= $travel->getId(); ?>"><button class="detailBtn">Reise buchen</button></a>
                    </div>

                    <h3>In dieser Zeit finden folgende Events statt</h3>
                    <ul>
                        <li>Drachenfest: 03.10.2018 Strand Lorem ipsum dolor sit amet.</li>
                        <li>Wattparade: 04.10.2018 odit error ducimus magni reiciendis est! Quisquam,</li>
                        <li>Pony-Sonntag: 7.10.2018 Lorem ipsum dolor sit amet, consectetur adipisicing.</li>
                    </ul>
                    <h3>Entfernungen zu spannenden Orten in der Umgebung</h3>
                    <ul>
                        <li>Strand: Hic sapiente ad perferendis aspernatur</li>
                        <li>Seerobben-Aufnahme: id in suscipit quas excepturi, rem modi,</li>

                    </ul>
                    molestiae quam ut doloribus
                        asperiores iure debitis facere voluptatum incidunt sequi. Explicabo nemo dignissimos suscipit
                        deserunt molestiae. Unde illum quasi nisi perspiciatis provident aut molestias eius eum tempore
                        culpa! Pariatur iste quia repellendus dolores commodi animi, consequatur ad praesentium incidunt,
                        ipsum nobis reiciendis voluptatibus necessitatibus.</p>',
        'thumbnail' => 'poolkids-thumbnail.jpg',
        'image' => 'poolkids2560.jpg',
        'travelstart' => new DateTime('2018-11-04'),
        'travelend' => new DateTime('2018-11-11'),
        'price' => 419.99,
        'cat' => 3,
        'region' => 9],
];

foreach ($travels as $input) {
    $travel = new Entities\Travel();
    $travel->setByArray($input);

    $category = $em->getRepository('\Entities\Category')->find($input['cat']);
    $travel->setCategory($category);

    $region = $em->getRepository('\Entities\Region')->find($input['region']);
    $travel->setRegion($region);


    $em->persist($travel);
}

$em->flush();

/* * ** TRAVELS END *** */

/* * ** BOOKING EXAMPLES *** */

$bookings = [
    [
        'persons' => '2',
        'bookingdate' => new DateTime('2018-08-21'),
        'salutation' => 'Herr',
        'firstname' => 'Max',
        'lastname' => 'Mustermann',
        'birthday' => new DateTime('1959-11-11'),
        'street' => 'Maxstraße',
        'streetnumber' => '25',
        'zip' => '22761',
        'city' => 'Hamburg',
        'phone' => '040 123456',
        'agb' => true,
        'email' => 'max.mustermann@gmx.de',
        'travel_id' => 1,
    ],
    [
        'persons' => '1',
        'bookingdate' => new DateTime('2018-09-27'),
        'salutation' => 'Frau',
        'firstname' => 'Inga',
        'lastname' => 'Schünemann',
        'birthday' => new DateTime('1985-10-11'),
        'street' => 'Heuertweg',
        'streetnumber' => '8',
        'zip' => '22179',
        'city' => 'Hamburg',
        'agb' => true,
        'phone' => '0151 70027085',
        'email' => 'schuenemann.inga@gmail.com',
        'travel_id' => 2,
    ],
    [
        'persons' => '2',
        'bookingdate' => new DateTime('2018-9-17'),
        'salutation' => 'Frau',
        'firstname' => 'Wenke',
        'lastname' => 'Schmidt',
        'birthday' => new DateTime('1968-07-07'),
        'street' => 'Rodigallee',
        'streetnumber' => '2',
        'zip' => '90411',
        'city' => 'Nürnberg',
        'agb' => true,
        'phone' => '0941 03404000',
        'email' => 'wenkeschmidt@t-online.de',
        'travel_id' => 3,
    ]
];

foreach ($bookings as $input) {
    $booking = new Entities\Booking();
    $booking->setByArray($input);

    $travel = $em->getRepository('\Entities\travel')->find($input['travel_id']);
    $booking->setTravel($travel);

    $em->persist($booking);
    $em->flush();
}