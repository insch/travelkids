<?php

namespace Controllers\Backend;

class UserController extends \Controllers\AbstractBase {

    public static function readCookie() {
        if (isset($_COOKIE['user'])) {
            $_SESSION['user'] = $_COOKIE['user'];
        } else {
            $this->redirect('login', 'user');
        }
    }

    //Validieren, ob User eingeloggt ist, um Daten zu sehen
    public static function isUserLoggedIn() {
        return(isset($_SESSION['user'])) ? true : false; //wenn ein User in der Session ist, dann true ausgeben, ansonsten false
    }

    public function indexAction() {
        if (self::isUserLoggedIn()) {
            $this->redirect('travellist', 'travel');
        } else {
            $this->redirect('admin', 'user');
        }
    }

    public function adminAction() {
        $error = filter_input(INPUT_GET, 'error');
        $this->addContext('error', $error);
        //Formular anzeigen
        $this->setTemplate('loginAction');
    }

    public function loginAction() {
        $form = filter_input_array(INPUT_POST);

        $user = $this->em->getRepository('Entities\User')->findOneByUsername($form['username']); //einen Wert, der der Eingabe entspricht finden

        if (($user instanceof\Entities\User) && password_verify($form['password'], $user->getPassword())) {
            $_SESSION['user'] = $user->getId();

            if (isset($form['stayLoggedIn'])) {
                setcookie('user', $user->getId(), time() + (60 * 60 * 24 * 30));
            }
            $this->redirect('travellist', 'travel');
        } else {
            $this->redirect('admin&error=login', 'user');
        }
    }

    public function logoutAction() {
        session_destroy();
        setcookie('username', '', time() - 3600); //Überschreiben des Cookies "user", nicht reinsetzen, aktuelle Zeit minus eine Stunde, damit Cookie gelöscht wird
        $this->redirect('admin', 'user');
    }

}
