<div class='content'>
    <h2>Liste aller Kategorien</h2>
    <section class='cats'>
        <?php foreach ($categories as $category) { ?>
            <div class='circle <?= strtolower($category->getTitle()); ?>'>
                <a href='<?= BASE_DIR; ?>/index.php?controller=category&action=catlist&id=<?= $category->getId(); ?>'>
                    <?= $category->getTitle(); ?>
                </a>
            </div>
        <?php } ?>
    </section>
</div>

