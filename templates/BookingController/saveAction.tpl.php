<div class='wrapperStatic'>
    <div class='content'>
        <main id='static'>
            <button id='back-to-Btn' type='button' onclick='history.back()'>zurück</button>
                     
            <h2>Bitte prüft eure Daten:</h2>
            
            <?php //echo '<pre>'; 
//            var_dump($_SESSION['bookinginfo']);
//            echo '</pre>';?>
           
            <!-- FORMULAR ZUR ANSICHT DER EINGERAGENEN DATEN -->
            <div class='bookingForm'>
                <form action='<?= BASE_DIR;?>/index.php?controller=booking&action=danke' method='post'>
                    
                    <input type='hidden' name='travelId' value="<?= $_SESSION['bookinginfo']['travelId']?>" />

                    <div class='row'>
                        <label for='salutation'>Anrede: </label>
                        <div>
                            <label class='radio' for='salutation'><input checked type='radio' class='radio' name='salutation' id='salutation' value='<?= $_SESSION['bookinginfo']['salutation']; ?>'/><?= $_SESSION['bookinginfo']['salutation']; ?></label>
                        </div>
                    </div>

                    <div class='row'>
                        <label for='firstname'>Vorname:</label>
                        <input disabled type='text' name='firstname' id='firstname' placeholder='Vorname' value='<?= $_SESSION['bookinginfo']['firstname'];?>'/>
                    </div>

                    <div class='row'>
                        <label for='lastname'>Nachname:</label>
                        <input disabled type='text' name='lastname' id='lastname' placeholder='Nachname' value='<?= $_SESSION['bookinginfo']['lastname'];?>'/>
                    </div>

                    <div class='row'>
                        <label for='email'>E-Mail-Adresse:</label>
                        <input disabled type='email' name='email' id='email' placeholder='E-Mail-Adresse' value='<?= $_SESSION['bookinginfo']['email'];?>'/>
                    </div>

                    <div class='row'>
                        <label for='birthday'>Geburtsdatum:</label>
                        <input disabled type='date' name='birthday' id='birthday' placeholder='tt.mm.jjjj' value='<?= $_SESSION['bookinginfo']['birthday'];?>'/>
                    </div>

                    <div class='row'>
                        <label for='phone'>Telefon-Nr.:</label>
                        <input disabled type='text' id='phone' name='phone' placeholder='Telefon-Nr.' value='<?= $_SESSION['bookinginfo']['phone'];?>'/>
                    </div>

                    <div class='row'>
                        <label for='street'>Straße:</label>
                        <input disabled type='text' name='street' id='street' placeholder='Straße' value='<?= $_SESSION['bookinginfo']['street'];?>' />
                    </div>
                    <div class='row'>
                        <label for='streetnumber'>Hausnummer:</label>
                        <input disabled type='text' name='streetnumber' id='streetnumber' placeholder='212a' value='<?= $_SESSION['bookinginfo']['streetnumber'];?>'/>
                    </div>

                    <div class='row'>
                        <label for='zip'>PLZ:</label>
                        <input disabled type='text' name='zip' id='zip'  value='<?= $_SESSION['bookinginfo']['zip'];?>' />
                    </div>

                    <div class='row'>
                        <label for='city'>Ort:</label>
                        <input disabled type='text' name='city' id='city' value='<?= $_SESSION['bookinginfo']['city'];?>' />
                    </div>

                    <div class='row'>
                        <label for='persons'>Anzahl der Reisenden:</label>
                        <input disabled type='number' name='persons' id='persons' min='1' max='8' value='<?= $_SESSION['bookinginfo']['persons'];?>'/>
                    </div>
                    
                    <div class='row'>
                        <label for='sum'>Gesamtpreis:</label>
                        <input disabled type='text' name='sum' id='sum' min='1' max='8' value='<?= $_SESSION['bookinginfo']['persons']*$travel->getPrice(). ' €'?>'/>
                    </div>
                    
                    
                    <div><input type="hidden" name="bookingdate" id="bookingdate" value="<?= $_SESSION['bookinginfo']['bookingdate'];?>" /></div>

                    <div class='row'>
                        <label for='agb'>AGB bestätigen:</label>
                        <div>
                            <input required id='agb' name='agb' type='checkbox' value='<?= (isset($booking)) ? clean($booking->getAgb()) : ''; ?>'/> Ich akzeptiere die AGB <a href='<?= BASE_DIR .'/index.php?controller=index&action=agb'?>'>(-> AGB lesen)</a> und habe sie gelesen.
                        </div>
                    </div>
                    <div class='row'>
                        <button type='submit' disabled id='kaufen' class='btns'>Reise kostenpflichtig buchen!</button>
                    </div>
                </form>
            </div>
            
            <h2>Kurz &amp; knapp - Reisezusammenfassung:</h2>
            <div class='kurzKnapp booking'>
                <div>
                    <h3><?= $travel->getTitle(); ?></h3>
                    <ul>
                        <i class='fa fa-calendar fa-2x' aria-hidden='true'></i><li>Reisezeit: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></li>
                        <i class='fa fa-map-marker fa-2x' aria-hidden='true'></i> <li> Region: <?= $travel->getRegion()->getName(); ?></li>
                        <i class='fa fa-users fa-2x' aria-hidden='true'></i> <li> max. Personen: 8</li>
                        <i class='fa fa-eur fa-2x' aria-hidden='true'></i> <li> Preis: <?= $travel->getPrice() . ' €'; ?> pro Person</li>
                    </ul>
                </div>
                <div>
                    <img src="<?= BASE_DIR?>/img/thumbnail/<?= $travel->getThumbnail(); ?>" alt="" />
                    <p></p>
                    <p>Kurzbeschreibung: <?= $travel->getTeaser(); ?></p>
                </div>
            </div>

            <h3>Allgemeine Infos zur Buchung:</h3>
            <p>Lorem ipsum E-Mail mit Buchungsbestätigung sit amet, consectetur Reiseunterlagen elit. Holoremque nisi voluptatibus officiis deleniti, repellat sequi, rem mollitia repellendus obcaecati eius. Eligendi sint assumenda excepturi incidunt necessitatibus error saepe, amet officia porro eos, enim, minus distinctio nemo tenetur ad nihil dignissimos doloribus iusto? Repellat ad at, praesentium dolores debitis explicabo, modi sapiente numquam excepturi non reprehenderit enim? At excepturi ea culpa ratione unde, quos nihil, ex praesentium nemo esse sed, provident, quibusdam sunt. Inventore nobis labore veritatis autem quo odit reprehenderit, consectetur iure molestias beatae deserunt nemo quos rem eveniet. Asperiores omnis ab labore illum aperiam. Harum amet tempore quae veritatis ipsum laudantium perferendis, rem placeat ad magnam repudiandae perspiciatis accusamus consequatur quisquam voluptatem eum assumenda, laborum quo aperiam debitis quaerat. Dignissimos cumque necessitatibus harum assumenda quisquam in temporibus earum quia reprehenderit alias atque, debitis, porro facilis illo veniam quae facere neque nobis omnis consequuntur ipsa vero laboriosam.</p>

        </main>
    </div>
</div>

<script>
    let agb = document.querySelector('#agb').selected;
    if(agb === false) {
        document.querySelector('#kaufen').disabled = true;
    } else {
        document.querySelector('#kaufen').disabled = false;
    }
</script>