<div class="wrapperStatic">
    <div class='content'>
        <main id='static'>
            <h2>Impressum</h2>
            <p><strong>Die Travelkids-Webseite www.travelkids.de wird betrieben von</strong></p>
            <p>Reisebüro Gerke &amp; Falter GbR<br /> Engelhardstraße 21<br />90402 Nürnberg</p>
            <p>Telefon: 091112345678<br /> Telefax: 091187654321<br /> E-Mail: <a href='mailto:info@travelkids.de'>info@travelkids.de</a><br />
            </p>
            <p><b>Vertreten durch:</b><br />Monika Gerke und Petra Falter</p>
            <p><b>Umsatzsteuer-ID: </b><br />Umsatzsteuer-Identifikationsnummer nach §27a Umsatzsteuergesetz:<br />DE 123456789</p>
            <p><b>Verantwortlich für den Inhalt</b> (gem. § 55 Abs. 2 RStV):<br />Monika Gerke</p>
            <br />
            <h3>Hinweis gemäß Online-Streitbeilegungs-Verordnung</h3>
            <p>Nach geltendem Recht sind wir verpflichtet, Verbraucher auf die Existenz der Europäischen Online-Streitbeilegungs-Plattform hinzuweisen, die für die Beilegung von Streitigkeiten genutzt werden kann, ohne dass ein Gericht eingeschaltet werden
                muss. Für die Einrichtung der Plattform ist die Europäische Kommission zuständig. Die Europäische Online-Streitbeilegungs-Plattform ist hier zu finden: <a href='http://ec.europa.eu/odr' target='_blank' rel='nofollow'>http://ec.europa.eu/odr</a>.
                Unsere E-Mail lautet: <a href='mailto:info@travelkids.de'>info@travelkids.de</a></p>
            <p>Wir weisen aber darauf hin, dass wir nicht bereit sind, uns am Streitbeilegungsverfahren im Rahmen der Europäischen Online-Streitbeilegungs-Plattform zu beteiligen. Nutzen Sie zur Kontaktaufnahme bitte unsere obige E-Mail und Telefonnummer.</p><br
                />
            <h3>Hinweis gemäß Verbraucherstreitbeilegungsgesetz (VSBG)</h3>
            <p>Wir sind nicht bereit und verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</p>

            <p>Bildquellen:<br> In einigen Fällen stammen Bilder in der Kategorie Reiseziele aus der Bilddatenbank www.pixelio.de</p>
            <p>Weitere Bildquellen:</p>
            <ul>
                <li>www.istockphoto.com</li>
                <li>www.fotolia.de</li>
                <li>www.pexels.de</li>
            </ul>
            <br>
        </main>
    </div>
</div>
