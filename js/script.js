"use strict";
{
    /* Back to former page */
    let goBack = () => {
        window.history.back();
    };
    /* Back to Top */
    window.addEventListener('scroll', () => {
        if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
            document.querySelector('#btnUp').style.display = 'block';
        } else {
            document.querySelector('#btnUp').style.display = 'none';
        }
    });
    document.querySelector('#btnUp').addEventListener('click', () => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });
    /* SUCHFELD */
    document.querySelector('#search').addEventListener('change', () => {
        let input = document.querySelector('#search').value;
        let option = document.querySelector('option').text;
        console.log('input ' + input);
        console.log('options ' + option);
//        if (input.indexOf('search') >= 0) {
//            window.location.href = 'index.php?controller=travel&action=searchId&id=' + input.replace('search', '');
        if (input === option) {
            window.location.href = 'index.php?controller=travel&action=search';
        } else if(input) {
            document.querySelector('#searchBtn').disabled = false;
        } 
    
    console.clear();
    });
}