<?php

namespace Controllers\Backend;

use Webmasters\Doctrine\ORM\Util;

class BookingController extends \Controllers\AbstractBase {

    public function indexAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
        $this->bookinglistAction();
        } else {
            $this->redirect('login', 'user');
        } 
    }

    //Liste aller Buchungn
    public function bookinglistAction() {
        
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $bookings = $this->em->getRepository('Entities\Booking')->findAll();
            $this->addContext('bookings', $bookings);
            $this->setTemplate('bookinglistAction');
        } else {
            $this->redirect('login', 'user');
        }
    }

//    public function deleteAction($id) {
//        
//        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
//        $booking = $this->em->getRepository('Entities\Booking')->find($id);
//
//        $this->em->remove($booking);
//        $this->em->flush();
//
//        $this->setTemplate('bookinglistAction');
//        } else {
//            $this->redirect('login', 'user');
//        }
//    }

}
