<div class="wrapperHome">

    <header id="header" class="homeBild">
        <?php require_once 'sliderAction.tpl.php'; ?>
    </header>

    <div class='content'>
        <main class='home'>
            <h1>Eure Reise einfach online buchen</h1>
            <p>Ihr sucht die passende Familien-Reise und benötigst Tipps? Ihr wollt einen günstigen Reisepreis und den besten Service? Ihr seid auf der Suche nach der richtigen Hotelanlage für euren Familien-Urlaub? Travelkids - hier seid ihr richtig.</p>
            <section class='top-reisen'>
                <?php
                if (count($travels)) {
                    foreach ($travels as $travel) { ?>
                                <article class='natur'>
                                    <a href="<?= BASE_DIR . '/index.php?controller=travel&action=traveldetail&id=' . $travel->getId(); ?>" title=''><img src='<?= BASE_DIR . '/img/thumbnail/' . $travel->getThumbnail(); ?>'><span>nur <?= $travel->getPrice() . '€'; ?></span></a>
                                    <h3 class='titel'><?= $travel->getTitle() ?></h3>
                                    <p>Reisedatum: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></p>
                                    <?php if (strlen($travel->getTeaser()) > 60) { ?>
                                        <p class='kurzBeschreibung'><?=
                                            substr($travel->getTeaser(), 0, 60) . '... <a href="index.php?controller=travel&action=traveldetail&id=' . $travel->getId() . '">mehr</a>';
                                        } else {
                                            ?>
                                        <p class='kurzBeschreibung'><?= $travel->getTeaser() . '... <a href="index.php?controller=travel&action=traveldetail&id=' . $travel->getId() . '">mehr</a>'; ?></p> 
                                    <?php } ?>
                                </article> 
                        <?php
                    } }
                ?>
            </section>
            <a href="<?= BASE_DIR .'/index.php?controller=travel&action=travellist'?>" title="Alle Reisen anzeigen"><button type="button" class="btns btn-l">Alle Reisen anzeigen</button></a>
            <h2>Reisen in die unterschiedlichen Richtungen</h2>
            <section class='regions'>
                <?php foreach ($regions as $region) { ?>
                    <div>
                        <a href='<?= BASE_DIR?>/index.php?controller=region&action=regionlist&id=<?= $region->getId(); ?>'>
                            <img src='img/pixel-regions/<?= $region->getRegionimage(); ?>' alt='<?= $region->getName(); ?>'><span><?= $region->getName(); ?></span>
                        </a>
                    </div>
                <?php } ?>
            </section>
            <h2>Travelkids - euer Familienreise-Spezialist mit fairen Preisen</h2>
            <div class='tkInfos'>
                <div id='tkInfosPlan'><span>Planen:</span><br>Reisen zu festen Reisedaten</div>
                <div id='tkInfosKids'><span>Kindgerecht:</span> <br>Gezielt auf kleine Bedürfnisse ausgelegt</div>
                <div id='tkInfosCare'><span>Sorglos:</span><br>Zeit zum Erholen und Erleben</div>
                <div id='tkInfosFits'><span>Passend:</span><br>Für 2 bis 6 Personen geeignet</div>
                <div id='tkInfosLocal'><span>Lokal:</span><br>Leicht erreichbar mit Sack und Pack</div>
                <div id='tkInfosDur'><span>Dauer:</span><br>Tagesausflüge oder längere Reisen</div>
            </div>
        </main>
        <aside class='aside home'>
            <h3>So erreicht ihr euer Ziel</h3>
            <div class='transport'>
                <div id='train'>
                    <a href='https://www.bahn.de' title='Bahn-Tickets auf Bahn.de'><i class='fa fa-train fa-3x' aria-hidden='true'></i></a>
                </div>
                <div id='bus'>
                    <a href='https://www.goeuro.de/' title='Bus-Tickets auf GoEuro.de'><i class='fa fa-bus fa-3x' aria-hidden='true'></i></a>
                </div>
                <div id='ship'>
                    <a href='https://www.aferry.de/' title='Fähre buchen'><i class='fa fa-ship fa-3x' aria-hidden='true'></i></a>
                </div>
                <div id='car'>
                    <a href='https://route.web.de/' title='Routenplaner'><i class='fa fa-bicycle fa-3x' aria-hidden='true'></i></a>
                </div>
                <div id='bicycle'>
                    <a href='https://www.biketrekking.de/' title='Fahrrad-Tour planen'><i class='fa fa-car fa-3x' aria-hidden='true'></i></a>
                </div>
            </div>
        </aside>
    </div>
</div>

<script>
    "use strict";
    {
    /* SLIDER movement */
//        let random = getRandomNumber(1, 5);
        let width, sliderCount, start, end, autoSlide;;
        let livePosition = 0;
        let liveIndex = 0;
        let btnPrev = document.querySelector('.btnPrev');
        let btnNext = document.querySelector('.btnNext');
    
//        let getRandomNumber = (min, max) => {
//            min = Math.ceil(min);
//            max = Math.floor(max);
//            return Math.floor(Math.random() * (max - min)) + min;
//        };
        
        let script = () => {
            setInitials();
            initialEvents();
            checkPositionStartEnding();
            createPointNav();
            setActivePointNavItem();
            setSliderInterval();
        };
        
        let setInitials = () => {
            width = document.querySelector("#header").offsetWidth;
            //width = getComputedStyle(document.querySelector('#header')).width;
            
            sliderCount = document.querySelectorAll('#moveIt').length;
           
            document.querySelector('.slider').style.width = (sliderCount * width) + 'px';
            start = 0;
            end = -((sliderCount - 1) * width);
            btnNext.style.display = 'block';
            btnPrev.style.display = 'block';
        };
        
        let initialEvents = () => {
            btnPrev.addEventListener('click', handleBtn);
            btnNext.addEventListener('click', handleBtn);
            window.addEventListener('resize', handleResize);
            document.querySelector('.slider').addEventListener('mouseenter', clearSliderInterval);
            document.querySelector('.slider').addEventListener('mouseleave', setSliderInterval);
        };
        
        let handleBtn = (event) => {
            console.log(event.target.classList.contains('btnNext'));
            if (event.target.classList.contains('btnNext')) {
                moveSlider(liveIndex - 1);
                console.log('index next ' +livePosition);
            } else {
                moveSlider(liveIndex + 1);
                console.log('index prev ' + livePosition);
            }
        };
        
        let handleResize = () => {
            setInitials();
            changePosition();
        };
    
        let changePosition = () => {
            moveSlider(liveIndex);
            console.log('changePosition liveIndex' + liveIndex);
        };
    
        let moveSlider = (index) => {
            liveIndex = index;
            livePosition = (liveIndex * width);
            console.log('livePosition ' + livePosition);
            
            document.querySelector('.slider').style.left = livePosition + 'px';
            
            checkPositionStartEnding();
            setActivePointNavItem();
        };
        
        let checkPositionStartEnding = () => {
            btnPrev.style.display = livePosition === start ? 'none' : 'block';
            btnNext.style.display = livePosition === end ? 'none' : 'block';
        };
        
        let createPointNav = () => {
            for (let i = 0; i < sliderCount; i++) {
                let span = document.createElement('span');
                let dataIndex = -i;
                if (i === start) {
                   span.classList.add('active');
                }

                span.setAttribute('data-index', dataIndex);

                span.addEventListener('click', (event) => {
                    moveSlider(parseInt(event.target.dataset.index));
                });
                
                document.querySelector("#point-nav").appendChild(span);
                
                console.log(span);
            }
        };
        
        let setActivePointNavItem = () => {
            document.querySelector('#point-nav .active').classList.remove('active');
            console.log(document.querySelector('#point-nav .active'));
            
            let index = Math.abs(livePosition / width);
            document.querySelectorAll('#point-nav span')[index].classList.add('active');
        };
        
        let setSliderInterval = () => {
            autoSlide = setInterval(() => {
                if (livePosition === end) {
                    moveSlider(start);
                } else {
                    moveSlider(liveIndex - 1);
                }
            }, 8000);
        };

        let clearSliderInterval = () => {
            clearInterval(autoSlide);
        };
        
        script();
    }
</script>