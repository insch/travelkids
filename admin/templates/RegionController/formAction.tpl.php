<div class='content'>
    <?php if (isset($region)) { ?>
        <h2>Region anpassen</h2>
    <?php } else { ?>
        <h2>Neue Region anlegen</h2>
    <?php } ?>

    <hr>
    <br>

    <form action="<?= BASE_DIR; ?>/admin/index.php?controller=region&action=save" method="post">

        <!-- EDIT fragen, ob die ID schon vorhanden/gesetzt ist -->
        <?php if (isset($region)) { ?>
            <input type="hidden" name="id" value="<?= $region->getId(); ?>" />
        <?php } ?>


        <div class="row">
            <label for="titel">Titel:</label>
            <input type="text" name="titel" id="titel" placeholder="Geben Sie den Titel der Region ein" value="<?= (isset($region)) ? $region->getTitle() : ''; ?>" />
            <label for="regionimage">Bild der Region:</label>
            <input required type="file" name="regionimage" id="regionimage" />

            <?php if (isset($region)) { ?>
                <img src="../../../img/pixel-regions/<?= $region->getRegionimage(); ?>" width="256" height="256" alt="<?= $region->getTitle(); ?>"/>
            <?php } else { ?>
                <p>Noch kein Bild vorhanden</p>
            <?php } ?>

            <?php if (isset($region)) { ?>
                <button type="submit" class="btn">Anpassen</button>
            <?php } else { ?>
                <button type="submit" class="btn">Hinzufügen</button>
            <?php } ?>
        </div>

    </form>
</div>
