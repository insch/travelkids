<footer id="footer">
    <ul>
        <li><a href='<?= BASE_DIR.'/index.php?controller=index&action=datenschutz';?>'>Datenschutz</a></li>
        <li><a href='<?= BASE_DIR.'/index.php?controller=index&action=impressum';?>'>Impressum | AGB</a></li>
        <li><a href='<?= BASE_DIR.'/index.php?controller=index&action=ueberuns';?>'>Über uns</a></li>
        <?php if (isset($_SESSION['user'])) { ?>
            <li><a href='<?= BASE_DIR.'/admin/index.php?controller=user&action=logout'?>'> Abmelden</a></li>
        <?php } ?>

            <?php if (!isset($_SESSION['user'])) { ?>
            <li><a href='<?= BASE_DIR.'/admin/index.php?controller=user&action=admin'?>'> Anmelden</a></li>
        <?php } ?>
    </ul>
</footer>
