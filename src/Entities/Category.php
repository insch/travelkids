<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Webmasters\Doctrine\ORM\Util;

/**
 * Doctrine\ORM\Mapping\Entity
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category extends AbstractEntity {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned" = true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     *
     * @ORM\OneToMany(targetEntity="Travel", mappedBy="category")
     */
    private $travels;
    
    public function __construct() {
       $this->travels = new \Doctrine\Common\Collections\ArrayCollection();
   }
    
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getTravels() {
       return $this->travels;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setTravels($travels) {
        $this->travels = $travels;
    }
}
