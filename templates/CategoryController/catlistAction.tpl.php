<div class="wrapperList">
    <h2>Unsere <?= $category->getTitle(); ?>-Reisen</h2>

    <?php
    if (count($category->getTravels())) {
        foreach ($category->getTravels() as $k => $travel) { ?>
            <?php $travelstart = ($travel->getTravelstart()->getDatetime()->getTimestamp()) ; 
            $now = (new \DateTime())->getTimestamp();
            if($travelstart > $now) { ?>
                <div class='reiseVorschau <?= strtolower($category->getTitle());?>'>
                    <div class='reiseImg'>
                        <a href="<?= BASE_DIR . '/index.php?controller=travel&action=traveldetail&id=' . $travel->getId(); ?>">
                            <img src="<?= BASE_DIR . '/img/thumbnail/' . $travel->getThumbnail(); ?>" alt=''/>
                            <span class='region'><?= $travel->getRegion()->getName(); ?></span>
                        </a>
                    </div>
                    <div class='reiseInfos'>
                        <h3><?= $travel->getTitle(); ?></h3>
                        <p><?= $travel->getTeaser(); ?></p>
                        <p>Reisezeit: <?= $travel->getTravelstart()->format('d.m.Y'); ?> - <?= $travel->getTravelend()->format('d.m.Y'); ?></p>
                        <p><a href='<?= BASE_DIR . '/index.php?controller=travel&action=traveldetail&id=' . $travel->getId(); ?>' title='<?= $travel->getTitle(); ?>'>Details - <?= $travel->getTitle(); ?></a></p>
                    </div>
                    <div class='reiseBtn'>
                        <div class='vorschauPreis'><?= $travel->getPrice() . '€ <br>pro Person'; ?></div>
                        <a href='<?= BASE_DIR . '/index.php?controller=booking&action=form&id=' . $travel->getId()?>' title='Reise buchen'><button class='btns btn-l'>Zur Buchung</button></a>
                    </div>
                </div>
        <?php } }
    } ?>
</div>