<?php

namespace Controllers\Backend;

use Webmasters\Doctrine\ORM\Util;

class CategoryController extends \Controllers\AbstractBase {

    public function indexAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $this->redirect('list', 'category');
        } else {
            $this->redirect('login', 'user');
        }
    }

    // admin/index.php?controller=category&action=list
    public function listAction() {
        if (\Controllers\Backend\UserController::isUserLoggedIn()) {
            $categories = $this->em->getRepository('\Entities\Category')->findAll();
            $this->addContext('categories', $categories);

            $this->setTemplate('listAction');
        } else {
            $this->redirect('login', 'user');
        }
    }

}
