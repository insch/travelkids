<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
        <!-- TinyMCE WYSIWYG Editor für die Texteingabe https://www.tinymce.com/download/-->
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
        <script>tinymce.init({selector: '.tinymce'});</script>
        <!-- Stylesheet -->
        <link rel="stylesheet" href="<?= BASE_DIR; ?>/src/css/style.css" />
        <title>Traumreise - Portal</title>
    </head>
    <body>

        <nav class="bg-info">
            <div class="container">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar navbar-nav">
                        <li><a href="<?= BASE_DIR; ?>"><span class="glyphicon glyphicon-home"></span></a></li>
                        <li><a href="<?= BASE_DIR; ?>/reise"> Reisen</a></li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="<?= BASE_DIR; ?>/benutzer">Mitarbeiterbereich <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= BASE_DIR; ?>/kategorie">Kategorie</a></li>
                                <li><a href="<?= BASE_DIR; ?>/region">Regionen</a></li>
                            </ul>
                        </li>
                    </ul>


                    <div class="navbar-form navbar-right">
                        <form action="<?= BASE_DIR ?>/reise/suche" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="search" list="reisesuche" autocomplete="off" placeholder="Suche..." value="<?= (isset($search)) ? $search : ''; ?>" />
                                <datalist id="reisesuche" >
                                    <?php if(isset($reisen)) { foreach($reisen as $reise) { ?>
                                    <option value="<?= $reise->getTitel().' - '.$reise->getKategorie()->getTitel().' - '.$reise->getRegion()->getName()?>">
                                    <?php }} ?>
                                    
                                </datalist>
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                            </div>
                        </form>
                    </div>

                    <?php if (isset($_SESSION['user'])) { ?>
                        <div class="nav navbar-right"><a href="<?= BASE_DIR ?>/benutzer/logout" class="btn btn-default">Logout</a></div>
                    <?php } ?>
                </div>
            </div>
        </nav>

        <div class="container">

            <?php
            if (isset($tpl)) {
                include $tpl;
            }
            ?>
        </div>
    </body>
</html>
