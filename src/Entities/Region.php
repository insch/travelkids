<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Webmasters\Doctrine\ORM\Util;

/**
 * Doctrine\ORM\Mapping\Entity
 * @ORM\Entity
 * @ORM\Table(name="regions")
 */
class Region extends AbstractEntity {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned" = true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;
    
     /**
     * @ORM\Column(type="string", length=150)
     */
    private $regionimage;
    
    /**
     * @ORM\OneToMany(targetEntity="Travel", mappedBy="region")
     */
    private $travels;
    
    

// GETTER / SETTER 
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }
    
    public function getRegionimage() {
        return $this->regionimage;
    }

    public function getTravels() {
        return $this->travels;
    }

//    public function setId($id) {
//        $this->id = $id;
//    }

    public function setName($name) {
        $this->name = $name;
    }
    
    public function setRegionimage($regionimage) {
        $this->regionimage = $regionimage;
    }

    public function setTravels($travels) {
        $this->travels = $travels;
    }
    
}
