<?php

require_once 'inc/bootstrap.inc.php';

$schemaTool = new \Doctrine\ORM\Tools\SchemaTool($em);
$factory = $em->getMetadataFactory();
$metadata = $factory->getAllMetadata();

try {
    $schemaTool->updateSchema($metadata);
    echo 'Das SchemaTool wurde durchlaufen!';
} catch(PDOException $e) {
    echo 'ACHTUNG: Bei der Aktualisierung des SchemaTools gab es ein Problem:';
    echo $e->getMessage().'<br>';
    if(preg_match("/Unknown database '(.*)'/", $e->getMessage(), $matches)) {
        die(
            sprintf(
                'Erstellen Sie die Datenbank %s mit der Kollation uft8_general_ci',
                $matches[1]
            )
        );
    }
}
?>
Das SchemaTool wurde erfolgreich durchlaufen.